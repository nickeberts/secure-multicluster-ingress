resource "google_gke_hub_feature" "mesh" {
  name     = "servicemesh"
  location = "global"
  project = var.project_id
  provider = google-beta
}

resource "google_gke_hub_feature" "mcs" {
  name     = "multiclusterservicediscovery"
  location = "global"
  project = var.project_id
  provider = google-beta
}