/**
 * Copyright 2018 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

variable "project_id" {
  description = "Project ID"
}

variable "network_name" {
  description = "VPC name"
  default = "vpc-prod"
}

variable "fleets" {
  type = list(object({
    region       = string
    env          = string
    num_clusters = number
    subnet = object({
      name = string
      cidr = string
    })
  }))
  default = [
    {
      region       = "us-west2"
      env          = "prod"
      num_clusters = 1
      subnet = {
        name = "us-west2"
        cidr = "10.1.0.0/17"
      }
    },
    {
      region       = "us-central1"
      env          = "prod"
      num_clusters = 3
      subnet = {
        name = "us-central1"
        cidr = "10.2.0.0/17"
      }
    },
  ]
}

# GKE Config (config cluster for ingress etc.)
variable "gke_config" {
  type = object({
    name    = string
    region  = string
    zone    = string
    env     = string
    network = string
    subnet = object({
      name               = string
      ip_range           = string
      ip_range_pods_name = string
      ip_range_pods      = string
      ip_range_svcs_name = string
      ip_range_svcs      = string
    })
  })
  default = {
    name    = "gke-config"
    region  = "us-central1"
    zone    = "us-central1-f"
    env     = "config"
    network = "vpc-prod"
    subnet = {
      name               = "us-central1-config"
      ip_range           = "10.10.0.0/20"
      ip_range_pods_name = "us-central1-config-pods"
      ip_range_pods      = "10.11.0.0/18"
      ip_range_svcs_name = "us-central1-config-svcs"
      ip_range_svcs      = "10.12.0.0/24"
    }
  }
}

