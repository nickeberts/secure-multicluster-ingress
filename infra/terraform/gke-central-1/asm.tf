resource "google_gke_hub_feature_membership" "gke-central-1-mesh" {
  location   = "global"
  project = var.project_id
  feature    = var.mesh_feature
  membership = module.hub.cluster_membership_id
  mesh {
    management = "MANAGEMENT_AUTOMATIC"
  }
  provider = google-beta
}