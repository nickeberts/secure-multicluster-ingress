#!/usr/bin/env bash

([[ $(gsutil ls | grep "gs://${PROJECT_ID}/") ]] || \
gsutil mb -p ${PROJECT_ID} gs://${PROJECT_ID}) && \
([[ $(gsutil versioning get gs://${PROJECT_ID} | grep Enabled) ]] || \
gsutil versioning set on gs://${PROJECT_ID})
