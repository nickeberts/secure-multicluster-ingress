_Last tested by @ameer00 on March 15th 2023_

# Configuring MultiClusterIngress to multiple GKE Services using Anthos Service Mesh

This tutorial shows how to create a single MultiClusterIngress (i.e. GCLB frontend) with multiple Services (i.e. backends) running in multiple namespaces on multiple GKE clusters. The tutorial covers the following use cases:

- Multicluster Ingress using external Google load balancers (XLB) through ASM Ingress to multiple Services. These are accessible through the internet.
- Multicluster Ingress using internal Google load balancers (ILB) through ASM Ingress to multiple Services. These are accessible internally from your VPC within the same region.

```mermaid
graph TD;
subgraph GCP
subgraph mesh
    subgraph A[gke-1]
        subgraph Aasmns[asm-gateways ns]
            Agwx([Gateway]) -.- Aingx[asm-ingressgateway-xlb]
            Agwi([Gateway]) -.- Aingi[asm-ingressgateway-ilb]
        end
        subgraph Aappns[app A ns]
            Aappvsx([VirtualService]) -.-|host based routing to Service| Aapp[Service A]
            Aappvsi([VirtualService]) -.-|host based routing to Service| Aapp[Service A]
        end
    end
    subgraph B[gke-2]
        subgraph Basmns[asm-gateways ns]
            Bgwx([Gateway]) -.- Bingx[asm-ingressgateway-xlb]
            Bgwi([Gateway]) -.- Bingi[asm-ingressgateway-ilb]
        end
        subgraph Bappns[app A ns]
            Bappvsx([VirtualService]) -.-|host based routing to Service| Bapp[Service A]
            Bappvsi([VirtualService]) -.-|host based routing to Service| Bapp[Service A]
        end
    end
end

subgraph C[gke-config]
    gwapi([Gateway API])
end

ca[Cloud Armor] ==> GCLB ===> Aingx & Bingx
Clienti[Client internal] --> ILB ---> Aingi & Bingi
Aingx ==> Aappvsx
Bingx ==> Bappvsx
Aingi --> Aappvsi
Bingi --> Bappvsi
gwapi -.->|configures MultiClusterIngress LB| GCLB
gwapi -.-> ILB
end
Client[Client external] ==> ca
classDef thickBox stroke-width:4px;
classDef noBox stroke-width:0px;
classDef dotted stroke-dasharray: 5 2;
class A,B,C thickBox;
class mesh,Aasmns,Aappns,Basmns,Bappns,gkeingressistions dotted;
class GCP noBox;
```

[[_TOC_]]

## Objective

- Create four GKE clusters. Three clusters are used to deploy applications while the fourth cluster, `gke-config`, is used to configure MultiClusterIngress.
- Register all cluster to an Environ so that you can utilize advanced features.
- Enable MCI on the `gke-config` cluster.
- Enable the Mesh feature on the project. This features allows you to deploy Anthos Service Mesh on GKE clusters.
- Install ASM on the three app clusters with ingress gateway.
- Deploy three sample applications - [Bank of Anthos](https://github.com/GoogleCloudPlatform/bank-of-anthos), [Online Boutique](https://github.com/GoogleCloudPlatform/microservices-demo), and [Whereami](https://github.com/GoogleCloudPlatform/kubernetes-engine-samples/tree/master/whereami) on the three app clusters in three different namespaces.
- Configure MCI with external Google load balancer.
  - Configure DNS names to access the `frontend` service of all the applications using Cloud Endpoints and create Google managed SSL certificates using the Cloud endpoints DNS names.
  - Configure a Cloud Armor security policy and rules.
  - Configure MCI to access Online Boutique, Bank of Anthos, and the Whereami applications through all clusters' `asm-ingressgateway`s using a single MultiClusterIngress (GCLB frontend).
  - Use Gateway and VirtualService resources on both clusters to send traffic to the `frontend` Services for all applications.
- Configure MCI with an internal Google load balancer (ILB).
  - Create a separate ASM ingress gateway for ILB.
  - Configure [Gateway API](https://cloud.google.com/kubernetes-engine/docs/concepts/gateway-api) and deploy Gateway resources on the `gke-config` cluster.
  - Configure `Gateway` and `HTTPRoute` resources to use the `gke-l7-rilb-mc` Gateway class (which represents an L7 HTTP ILB).
  - Configure `Gateway` and `VirtualService` resources for the Whereami application.
  - Access application using a GCE instance in the same VPC.
- Configure locality load balancing
  - Test locality load balancing with both XLB and ILB multicluster ingress.
  - Test failure scenarios with locality load balancing.
- Clean up.

## Documentation

This tutorial uses the following documents:

- [ASM Install](https://cloud.google.com/service-mesh/docs/scripted-install/gke-install)
- [Multicluster Ingress](https://cloud.google.com/kubernetes-engine/docs/how-to/multi-cluster-ingress)
- [From egde to mesh](https://cloud.google.com/solutions/exposing-service-mesh-apps-through-gke-ingress#mesh_ingress_gateway)
- [Service Mesh Ingress](https://istio.io/latest/docs/tasks/traffic-management/ingress/ingress-control/)
- [Gateway API](https://cloud.google.com/kubernetes-engine/docs/concepts/gateway-api)

## Setting up the environment

1. Create envars.

   ```bash
   # Enter your project ID below
   export PROJECT_ID=YOUR PROJECT ID HERE
   # Copy and paste the rest below
   gcloud config set project ${PROJECT_ID}
   export PROJECT_NUM=$(gcloud projects describe ${PROJECT_ID} --format='value(projectNumber)')
   export CLUSTER_1=gke-west-1
   export CLUSTER_2=gke-central-1
   export CLUSTER_3=gke-central-2
   export CLUSTER_1_ZONE=us-west2-a
   export CLUSTER_2_ZONE=us-central1-a
   export CLUSTER_3_ZONE=us-central1-b
   export CLUSTER_1_REGION=us-west2
   export CLUSTER_2_REGION=us-central1
   export CLUSTER_3_REGION=us-central1
   export CLUSTER_CONFIG=gke-config
   export CLUSTER_CONFIG_ZONE=us-central1-f
   export WORKLOAD_POOL=${PROJECT_ID}.svc.id.goog
   export MESH_ID="proj-${PROJECT_NUM}"
   export ASM_LABEL=asm-managed
   export ASM_RELEASE_CHANNEL=regular
   export GSA_NAME=app-gsa
   export KSA_NAME=bank-ksa
   export BANK_DNS_PREFIX=bank
   export SHOP_DNS_PREFIX=shop
   export WHEREAMI_DNS_PREFIX=whereami
   export PROXY_ONLY_SUBNET=proxy-only-subnet
   export PROXY_ONLY_CIDR=10.100.0.0/20
   export ILB_REGION=us-central1
   export VPC=vpc-prod
   export ISTIOCTL_VER=1.16.2-asm.2
   ```

## Preparing the environment

1. Create a WORKDIR folder. All files created for this tutorial are placed in this folder.

   ```bash
   mkdir -p asm-mci && cd asm-mci && export WORKDIR=`pwd`
   git clone https://gitlab.com/asm7/secure-multicluster-ingress.git asm
   ```
1. Terraform is used to deploy GCP resources using Cloud Build. Cloud Build uses a GCP Service Account to run Teraform. Configure the Cloud Build GCP Service Account with IAM `role/owner`.

   ```bash
   export CLOUDBUILD_SA="${PROJECT_NUM}@cloudbuild.gserviceaccount.com"
   gcloud services enable cloudbuild.googleapis.com --project ${PROJECT_ID}
   gcloud projects add-iam-policy-binding "${PROJECT_ID}" \
     --member serviceAccount:"${CLOUDBUILD_SA}" \
     --role roles/owner
   ```

1. Create a Cloud Storage bucket for Terraform state.

   ```bash
   cd ${WORKDIR}/asm/infra/terraform/backend
   ./gcs.sh
   ```
   This script creates a Cloud Storage bucket called your `$PROJECT_ID` and enables versioning on the bucket.

## Enabling APIs

1. Enable the required APIs

   ```bash
   cd ${WORKDIR}/asm/infra/terraform/api
   gcloud builds submit --config cloudbuild.yaml
   ```
   This step enables the required APIs for this tutorial. You can view the build in the Console at the following link.

   ```bash
   https://console.cloud.google.com/cloud-build
   ```


## Enabling Multi Cluster Service and Mesh Features

1. Enable the `multiclusterservicediscovery` and `mesh` Fleet features on the project.

   ```bash
   cd ${WORKDIR}/asm/infra/terraform/fleet
   gcloud builds submit --config cloudbuild.yaml
   ```

   You can view the build in the Console at the following link.

   ```bash
   https://console.cloud.google.com/cloud-build
   ```

1. Create a VPC and subnets for GKE clusters.

   ```bash
   cd ${WORKDIR}/asm/infra/terraform/vpc
   gcloud builds submit --config cloudbuild.yaml
   ```
   This step creates a VPC called `vpc-prod` with subnets required for GKE clusters. You can view the build in the Console at the following link.

   ```bash
   https://console.cloud.google.com/cloud-build
   ```

1. Create GKE clusters.

   ```bash
   cd ${WORKDIR}/asm/infra/terraform/gke-west-1
   gcloud builds submit --config cloudbuild.yaml --async

   cd ${WORKDIR}/asm/infra/terraform/gke-central-1
   gcloud builds submit --config cloudbuild.yaml --async

   cd ${WORKDIR}/asm/infra/terraform/gke-central-2
   gcloud builds submit --config cloudbuild.yaml --async

   cd ${WORKDIR}/asm/infra/terraform/gke-config
   gcloud builds submit --config cloudbuild.yaml --async
   ```
   This step creates 4 GKE clusters named `gke-west-1` (in `us-west2` region), `gke-central-1` and `gke-central-2` (in `us-central1` region). These three clusters are used for applications. The fourth cluster is called `gke-config` (also in `us-central1` region) and is used to configure multi cluster ingress. This step also registers all clusters to Fleet and enables ASM on the application clusters. The step also enables the `ingress` feature on the `gke-config` cluster.

   You can view the build in the Console at the following link.

   ```bash
   https://console.cloud.google.com/cloud-build
   ```

   > If any of the Cloud Build jobs fail (sometimes due to unforseen Terraform issues), you can re run them. Ensure they all pass before proceeding.

1. Confirm clusters are `RUNNING`. Wait a few minutes before running the following command until the cluster creation has started.

   ```bash
   until [[ $(gcloud container clusters list --filter="name:${CLUSTER_1}" --format='value(status)') = "RUNNING" ]]; do date; sleep 5; echo ""; done
   echo -e "${CLUSTER_1} is RUNNING."
   until [[ $(gcloud container clusters list --filter="name:${CLUSTER_2}" --format='value(status)') = "RUNNING" ]]; do date; sleep 5; echo ""; done
   echo -e "${CLUSTER_2} is RUNNING."
   until [[ $(gcloud container clusters list --filter="name:${CLUSTER_3}" --format='value(status)') = "RUNNING" ]]; do date; sleep 5; echo ""; done
   echo -e "${CLUSTER_3} is RUNNING."
   until [[ $(gcloud container clusters list --filter="name:${CLUSTER_CONFIG}" --format='value(status)') = "RUNNING" ]]; do date; sleep 5; echo ""; done
   echo -e "${CLUSTER_CONFIG} is RUNNING."
   ```

   The output is similar to the following:

   ```
   gke-west-1 is RUNNING.
   gke-central-1 is RUNNING.
   gke-central-2 is RUNNING.
   gke-config is RUNNING.
   ```

1. Connect to clusters.

   ```bash
   touch ${WORKDIR}/asm-kubeconfig && export KUBECONFIG=${WORKDIR}/asm-kubeconfig
   gcloud container clusters get-credentials ${CLUSTER_1} --zone ${CLUSTER_1_ZONE}
   gcloud container clusters get-credentials ${CLUSTER_2} --zone ${CLUSTER_2_ZONE}
   gcloud container clusters get-credentials ${CLUSTER_3} --zone ${CLUSTER_3_ZONE}
   gcloud container clusters get-credentials ${CLUSTER_CONFIG} --zone ${CLUSTER_CONFIG_ZONE}
   ```

   > Remember to unset your `KUBECONFIG` var at the end.

1. Rename cluster context for easy switching.

   ```bash
   kubectl config rename-context gke_${PROJECT_ID}_${CLUSTER_1_ZONE}_${CLUSTER_1} ${CLUSTER_1}
   kubectl config rename-context gke_${PROJECT_ID}_${CLUSTER_2_ZONE}_${CLUSTER_2} ${CLUSTER_2}
   kubectl config rename-context gke_${PROJECT_ID}_${CLUSTER_3_ZONE}_${CLUSTER_3} ${CLUSTER_3}
   kubectl config rename-context gke_${PROJECT_ID}_${CLUSTER_CONFIG_ZONE}_${CLUSTER_CONFIG} ${CLUSTER_CONFIG}
   ```

1. Confirm all cluster contextx are present.

   ```bash
   kubectl config get-contexts -o name
   ```

   The output is similar to the following:

   ```
   gke-central-1
   gke-central-2
   gke-config
   gke-west-1
   ```

### Verifying multi cluster services, mesh and ingress features

1. Verify all clusters are registered to Fleet. If you see no items, wait a few moments and re run the command until you see all four clusters registered.

   ```bash
   gcloud container hub memberships list
   ```

   The output is similar to the following:

   ```
   NAME: gke-central-1
   EXTERNAL_ID: 374d6536-349b-496c-93f9-73aaf0c3a000
   LOCATION: global

   NAME: gke-west-1
   EXTERNAL_ID: c76e5bd3-cd5a-4dd9-9860-b979959cd2a2
   LOCATION: global

   NAME: gke-central-2
   EXTERNAL_ID: ecaf9fbf-7c52-4df5-914e-0c0e32289199
   LOCATION: global

   NAME: gke-config
   EXTERNAL_ID: 2a8bacb6-9e68-4fec-b8ec-dac90309bc6b
   LOCATION: global
   ```

1. Describe the `multi-cluster-service` feature. Wait until MCS feature is enabled on all clusters.

   ```bash
   gcloud container fleet multi-cluster-services describe --project ${PROJECT_ID}
   ```

   The output is similar to the following:

    ```
    createTime: '2023-03-16T01:42:22.164676883Z'
    membershipStates:
      projects/1061763066990/locations/global/memberships/gke-central-1:
        state:
          code: OK
          description: Firewall successfully updated
          updateTime: '2023-03-16T02:06:05.023435705Z'
      projects/1061763066990/locations/global/memberships/gke-central-2:
        state:
          code: OK
          description: Firewall successfully updated
          updateTime: '2023-03-16T02:06:20.202362963Z'
      projects/1061763066990/locations/global/memberships/gke-config:
        state:
          code: OK
          description: Firewall successfully updated
          updateTime: '2023-03-16T01:51:24.881599562Z'
      projects/1061763066990/locations/global/memberships/gke-west-1:
        state:
          code: OK
          description: Firewall successfully updated
          updateTime: '2023-03-16T02:06:14.115234983Z'
    name: projects/qwiklabs-gcp-01-dac3818e35be/locations/global/features/multiclusterservicediscovery
    resourceState:
      state: ACTIVE
    spec: {}
    updateTime: '2023-03-16T02:06:23.172979448Z'
    ```

1. Verify that the Gateway API resources are successfully deployed.

   ```bash
   # Wait until the CRDs are established, which could take a minute or two
   kubectl --context=${CLUSTER_CONFIG} wait --for=condition=established crd gatewayclasses.gateway.networking.k8s.io --timeout=5m

   # Verify resources are present
   kubectl --context=${CLUSTER_CONFIG} get gatewayclasses
   ```

   The output should be similar to the following:

   ```
   NAME          CONTROLLER                  AGE
   gke-l7-global-external-managed      networking.gke.io/gateway   True       18m
   gke-l7-global-external-managed-mc   networking.gke.io/gateway   True       3m45s
   gke-l7-gxlb                         networking.gke.io/gateway   True       18m
   gke-l7-gxlb-mc                      networking.gke.io/gateway   True       3m45s
   gke-l7-rilb                         networking.gke.io/gateway   True       18m
   gke-l7-rilb-mc                      networking.gke.io/gateway   True       3m45s
   ```

1. Verify ingress feature state.

   ```bash
   gcloud container fleet ingress describe --project=${PROJECT_ID}
   ```

   The output is similar to the following:

    ```
    membershipStates:
      projects/1061763066990/locations/global/memberships/gke-central-1:
        state:
          code: OK
          updateTime: '2023-03-16T02:10:05.077440368Z'
      projects/1061763066990/locations/global/memberships/gke-central-2:
        state:
          code: OK
          updateTime: '2023-03-16T02:10:05.077442883Z'
      projects/1061763066990/locations/global/memberships/gke-config:
        state:
          code: OK
          updateTime: '2023-03-16T02:10:05.077435739Z'
      projects/1061763066990/locations/global/memberships/gke-west-1:
        state:
          code: OK
          updateTime: '2023-03-16T02:10:05.077441672Z'
    name: projects/qwiklabs-gcp-01-dac3818e35be/locations/global/features/multiclusteringress
    resourceState:
      state: ACTIVE
    spec:
      multiclusteringress:
        configMembership: projects/qwiklabs-gcp-01-dac3818e35be/locations/global/memberships/gke-config
    state:
      state:
        code: OK
        description: Ready to use
    ```

1. Verify that ASM is installed on the three application clusters: `gke-west-1`, `gke-central-1` and `gke-central-2`.

   ```bash
   gcloud container fleet mesh describe --project=${PROJECT_ID}
   ```

   The output is similar to the following:

    ```
    membershipSpecs:
      projects/1061763066990/locations/global/memberships/gke-central-1:
        mesh:
          management: MANAGEMENT_AUTOMATIC
      projects/1061763066990/locations/global/memberships/gke-central-2:
        mesh:
          management: MANAGEMENT_AUTOMATIC
      projects/1061763066990/locations/global/memberships/gke-west-1:
        mesh:
          management: MANAGEMENT_AUTOMATIC
    membershipStates:
      projects/1061763066990/locations/global/memberships/gke-central-1:
        servicemesh:
          controlPlaneManagement:
            details:
            - code: REVISION_READY
              details: 'Ready: asm-managed'
            state: ACTIVE
          dataPlaneManagement:
            details:
            - code: OK
              details: Service is running.
            state: ACTIVE
        state:
          code: OK
          description: 'Revision(s) ready for use: asm-managed.'
          updateTime: '2023-03-16T02:13:00.873085579Z'
      projects/1061763066990/locations/global/memberships/gke-central-2:
        servicemesh:
          controlPlaneManagement:
            details:
            - code: REVISION_READY
              details: 'Ready: asm-managed'
            state: ACTIVE
          dataPlaneManagement:
            details:
            - code: OK
              details: Service is running.
            state: ACTIVE
        state:
          code: OK
          description: 'Revision(s) ready for use: asm-managed.'
          updateTime: '2023-03-16T02:13:00.944616576Z'
      projects/1061763066990/locations/global/memberships/gke-config:
        servicemesh:
          controlPlaneManagement:
            state: DISABLED
          dataPlaneManagement:
            details:
            - code: DISABLED
              details: Data Plane Management is not enabled.
            state: DISABLED
        state:
          code: OK
          description: Please see https://cloud.google.com/service-mesh/docs/install for
            instructions to onboard to Anthos Service Mesh.
          updateTime: '2023-03-16T02:13:00.872981610Z'
      projects/1061763066990/locations/global/memberships/gke-west-1:
        servicemesh:
          controlPlaneManagement:
            details:
            - code: REVISION_READY
              details: 'Ready: asm-managed'
            state: ACTIVE
          dataPlaneManagement:
            details:
            - code: OK
              details: Service is running.
            state: ACTIVE
        state:
          code: OK
          description: 'Revision(s) ready for use: asm-managed.'
          updateTime: '2023-03-16T02:14:20.692160430Z'
    name: projects/qwiklabs-gcp-01-dac3818e35be/locations/global/features/servicemesh
    resourceState:
      state: ACTIVE
    ```

   > It takes a few minutes for the controllers to configure MCI and Mesh in the GKE clusters. You may have to run the describe command a few times. Do not proceed until you see the output shown. If you see a `REVISION_STALLED` message for ASM, wait a few moments and it should resolve itself. If it does not resolve itself after a while, please contact support.

1. Verify that the two CRDs for Multicluster Ingress (MCI) (MultiClusterIngress and MultiClusterService) and the CRD for ASM (ControlPlaneRevision) have been deployed.

   ```bash
   # Multicluster Ingress CRDs in the Config cluster
   kubectl --context=${CLUSTER_CONFIG} wait --for=condition=established crd multiclusteringresses.networking.gke.io --timeout=5m
   kubectl --context=${CLUSTER_CONFIG} wait --for=condition=established crd multiclusterservices.networking.gke.io --timeout=5m

   # Anthos Service Mesh CRD in the app clusters
   kubectl --context=${CLUSTER_1} wait --for=condition=established crd controlplanerevisions.mesh.cloud.google.com --timeout=5m
   kubectl --context=${CLUSTER_2} wait --for=condition=established crd controlplanerevisions.mesh.cloud.google.com --timeout=5m
   kubectl --context=${CLUSTER_3} wait --for=condition=established crd controlplanerevisions.mesh.cloud.google.com --timeout=5m
   ```

   The output is similar to the following:

   ```
   customresourcedefinition.apiextensions.k8s.io/multiclusteringresses.networking.gke.io condition met
   customresourcedefinition.apiextensions.k8s.io/multiclusterservices.networking.gke.io condition met
   customresourcedefinition.apiextensions.k8s.io/controlplanerevisions.mesh.cloud.google.com condition met
   customresourcedefinition.apiextensions.k8s.io/controlplanerevisions.mesh.cloud.google.com condition met
   customresourcedefinition.apiextensions.k8s.io/controlplanerevisions.mesh.cloud.google.com condition met
   ```

## Configuring ASM

1. Enable Envoy access logs.

   ```bash
   cat <<EOF > ${WORKDIR}/asm-access-logs.yaml
   apiVersion: v1
   kind: ConfigMap
   metadata:
     name: istio-asm-managed
     namespace: istio-system
   data:
     mesh: |
       accessLogFile: /dev/stdout
       accessLogEncoding: JSON
   EOF

   kubectl --context=${CLUSTER_1} apply -f ${WORKDIR}/asm-access-logs.yaml
   kubectl --context=${CLUSTER_2} apply -f ${WORKDIR}/asm-access-logs.yaml
   kubectl --context=${CLUSTER_3} apply -f ${WORKDIR}/asm-access-logs.yaml
   ```

## Deploying Applications

In the next sections, you deploy three applications in your service mesh on all application clusters.

### Bank of Anthos

In this section, you deploy Bank of Anthos to all three application clusters.

1. Clone the Bank of Anthos repo.

   ```bash
   git clone https://github.com/GoogleCloudPlatform/bank-of-anthos.git ${WORKDIR}/bank-of-anthos
   ```

1. Create and label `bank-of-anthos` namespace in all clusters. The ASM label ensures that the Envoy proxies get automatically injected into each Pod in that namespace.

   ```bash
   cat <<EOF > ${WORKDIR}/namespace-bank-of-anthos.yaml
   apiVersion: v1
   kind: Namespace
   metadata:
     name: bank-of-anthos
     labels:
       istio.io/rev: ${ASM_LABEL}
   EOF

   kubectl --context=${CLUSTER_1} apply -f ${WORKDIR}/namespace-bank-of-anthos.yaml
   kubectl --context=${CLUSTER_2} apply -f ${WORKDIR}/namespace-bank-of-anthos.yaml
   kubectl --context=${CLUSTER_3} apply -f ${WORKDIR}/namespace-bank-of-anthos.yaml
   ```

1. Create a Google Service Account to set it up with Workload Identity. With Workload Identity, Kubernetes Service Accounts can authenticate and access Google APIs. Bank of Anthos requires access to Cloud Ops for metrics and traces.

   ```bash
   # Create a GCP SA with the required IAM roles
   gcloud iam service-accounts create ${GSA_NAME}

   gcloud projects add-iam-policy-binding ${PROJECT_ID} \
   --member "serviceAccount:${GSA_NAME}@${PROJECT_ID}.iam.gserviceaccount.com" \
   --role roles/cloudtrace.agent

   gcloud projects add-iam-policy-binding ${PROJECT_ID} \
   --member "serviceAccount:${GSA_NAME}@${PROJECT_ID}.iam.gserviceaccount.com" \
   --role roles/monitoring.metricWriter

   gcloud iam service-accounts add-iam-policy-binding \
   --role roles/iam.workloadIdentityUser \
   --member "serviceAccount:${PROJECT_ID}.svc.id.goog[bank-of-anthos/${KSA_NAME}]" \
   ${GSA_NAME}@${PROJECT_ID}.iam.gserviceaccount.com
   ```

1. Create a Kubernetes Service Account and associate the Workload Identity service account.

   ```bash
   cat <<EOF > ${WORKDIR}/serviceaccount-bank-of-anthos.yaml
   apiVersion: v1
   kind: ServiceAccount
   metadata:
     name: ${KSA_NAME}
     namespace: bank-of-anthos
     annotations:
       iam.gke.io/gcp-service-account: ${GSA_NAME}@${PROJECT_ID}.iam.gserviceaccount.com
   EOF

   kubectl --context=${CLUSTER_1} apply -f ${WORKDIR}/serviceaccount-bank-of-anthos.yaml
   kubectl --context=${CLUSTER_2} apply -f ${WORKDIR}/serviceaccount-bank-of-anthos.yaml
   kubectl --context=${CLUSTER_3} apply -f ${WORKDIR}/serviceaccount-bank-of-anthos.yaml
   ```

1. Prepare the Bank of Anthos application to use Workload Identity.

   ```bash
   mkdir -p ${WORKDIR}/wi-kubernetes-manifests
   for f in $(find ${WORKDIR}/bank-of-anthos/kubernetes-manifests -type f); do
       echo "Processing $f..."
       sed -e "s/serviceAccountName: default/serviceAccountName: ${KSA_NAME}/g" -e "s/0.4.3/0.4.2/g" $f > ${WORKDIR}/wi-kubernetes-manifests/`basename $f`
   done
   ```

1. Deploy Bank of Anthos application to all application cluisters in the `bank-of-anthos` namespace. Deploy the two PostGres DBs only in one cluster by deleting the StatefulSets from the other clusters.

   ```bash
   kubectl --context=$CLUSTER_1 -n bank-of-anthos apply -f ${WORKDIR}/bank-of-anthos/extras/jwt/jwt-secret.yaml
   kubectl --context=$CLUSTER_2 -n bank-of-anthos apply -f ${WORKDIR}/bank-of-anthos/extras/jwt/jwt-secret.yaml
   kubectl --context=$CLUSTER_3 -n bank-of-anthos apply -f ${WORKDIR}/bank-of-anthos/extras/jwt/jwt-secret.yaml
   kubectl --context=$CLUSTER_1 -n bank-of-anthos apply -f ${WORKDIR}/wi-kubernetes-manifests
   kubectl --context=$CLUSTER_2 -n bank-of-anthos apply -f ${WORKDIR}/wi-kubernetes-manifests
   kubectl --context=$CLUSTER_3 -n bank-of-anthos apply -f ${WORKDIR}/wi-kubernetes-manifests
   kubectl --context=$CLUSTER_1 -n bank-of-anthos apply -f ${WORKDIR}/bank-of-anthos/istio-manifests
   kubectl --context=$CLUSTER_2 -n bank-of-anthos apply -f ${WORKDIR}/bank-of-anthos/istio-manifests
   kubectl --context=$CLUSTER_3 -n bank-of-anthos apply -f ${WORKDIR}/bank-of-anthos/istio-manifests


   # Delete DBs from Cluster_2. and Cluster_3
   kubectl --context=$CLUSTER_2 -n bank-of-anthos delete statefulset accounts-db
   kubectl --context=$CLUSTER_2 -n bank-of-anthos delete statefulset ledger-db
   kubectl --context=$CLUSTER_3 -n bank-of-anthos delete statefulset accounts-db
   kubectl --context=$CLUSTER_3 -n bank-of-anthos delete statefulset ledger-db
   ```

   All Services are deployed as Distributed Services. This means that there are Deployments of all Services on both clusters. The `accounts-db` and `ledger-db` is only deployed to CLUSTER1.

### Online Boutique

In this section, you deploy Online Boutique to all three application clusters.

1. Clone the Online Boutique repo.

   ```bash
   git clone https://github.com/GoogleCloudPlatform/microservices-demo ${WORKDIR}/online-boutique
   ```

1. Create and label `online-boutique` namespace in both cluster. The ASM label ensures that the Envoy proxies get automatically injected into each Pod in that namespace.

   ```bash
   cat <<EOF > ${WORKDIR}/namespace-online-boutique.yaml
   apiVersion: v1
   kind: Namespace
   metadata:
     name: online-boutique
     labels:
       istio.io/rev: ${ASM_LABEL}
   EOF

   kubectl --context=${CLUSTER_1} apply -f ${WORKDIR}/namespace-online-boutique.yaml
   kubectl --context=${CLUSTER_2} apply -f ${WORKDIR}/namespace-online-boutique.yaml
   kubectl --context=${CLUSTER_3} apply -f ${WORKDIR}/namespace-online-boutique.yaml
   ```

1. Deploy Online Boutique to all application cluisters in the `online-boutique` namespace. Deploy `redis-cart` Deployment only in one cluster. Redis is used by `cartservice` to store items in your shopping cart. It is the only stateful service in Online Boutique.

   ```bash
   kubectl --context=$CLUSTER_1 -n online-boutique apply -f ${WORKDIR}/online-boutique/release
   kubectl --context=$CLUSTER_2 -n online-boutique apply -f ${WORKDIR}/online-boutique/release
   kubectl --context=$CLUSTER_3 -n online-boutique apply -f ${WORKDIR}/online-boutique/release

   # Delete redis-cart from Cluster_2 and Cluster_3
   kubectl --context=$CLUSTER_2 -n online-boutique delete deployment redis-cart
   kubectl --context=$CLUSTER_3 -n online-boutique delete deployment redis-cart
   ```

   All Services are deployed as Distributed Services. This means that there are Deployments of all Services on all clusters. The `redis-cart` is only deployed to CLUSTER_1.

### Whereami

In this section, you deploy the whereami application to all three application clusters.

1. Create and label `whereami` namespace in all application clusters. The ASM label ensures that the Envoy proxies get automatically injected into each Pod in that namespace.

   ```bash
   cat <<EOF > ${WORKDIR}/namespace-whereami.yaml
   apiVersion: v1
   kind: Namespace
   metadata:
     name: whereami
     labels:
       istio.io/rev: ${ASM_LABEL}
   EOF

   kubectl --context=${CLUSTER_1} apply -f ${WORKDIR}/namespace-whereami.yaml
   kubectl --context=${CLUSTER_2} apply -f ${WORKDIR}/namespace-whereami.yaml
   kubectl --context=${CLUSTER_3} apply -f ${WORKDIR}/namespace-whereami.yaml
   ```

1. Deploy whereami app to both cluisters in the `whereami` namespace.

   ```bash
   git clone https://github.com/GoogleCloudPlatform/kubernetes-engine-samples.git ${WORKDIR}/kubernetes-engine-samples
   kubectl apply -k ${WORKDIR}/kubernetes-engine-samples/whereami/k8s-backend-overlay-example/ --context=${CLUSTER_1} -n whereami
   kubectl apply -k ${WORKDIR}/kubernetes-engine-samples/whereami/k8s-backend-overlay-example/ --context=${CLUSTER_2} -n whereami
   kubectl apply -k ${WORKDIR}/kubernetes-engine-samples/whereami/k8s-backend-overlay-example/ --context=${CLUSTER_3} -n whereami
   kubectl apply -k ${WORKDIR}/kubernetes-engine-samples/whereami/k8s-frontend-overlay-example/ --context=${CLUSTER_1} -n whereami
   kubectl apply -k ${WORKDIR}/kubernetes-engine-samples/whereami/k8s-frontend-overlay-example/ --context=${CLUSTER_2} -n whereami
   kubectl apply -k ${WORKDIR}/kubernetes-engine-samples/whereami/k8s-frontend-overlay-example/ --context=${CLUSTER_3} -n whereami
   ```

## Verifying Applications

In this section you verify that all applications are running on all clusters.

### Verifying Bank of Anthos

1. Verify that all Pods are running in all clusters.

   ```bash
   export SERVICES=(contacts frontend ledgerwriter loadgenerator balancereader transactionhistory userservice)
   for CLUSTER in $CLUSTER_1 $CLUSTER_2 $CLUSTER_3; do
     echo -e "\nChecking deployments in $CLUSTER cluster..."
     for SVC in "${SERVICES[@]}"; do
       kubectl --context=${CLUSTER} -n bank-of-anthos wait --for=condition=available --timeout=5m deployment $SVC
     done
   done

   # Checking DBs in Cluster_1 only
   kubectl --context=${CLUSTER_1} -n bank-of-anthos wait --for=condition=ready --timeout=5m pod accounts-db-0
   kubectl --context=${CLUSTER_1} -n bank-of-anthos wait --for=condition=ready --timeout=5m pod ledger-db-0
   ```

   The output is similar to the following:

   ```
   ...
   deployment.apps/frontend condition met # For all deployments
   ...
   ```

### Verifying Online Boutique

1. Verify that all Pods are running in all clusters.

   ```bash
   unset SERVICES
   SERVICES=(adservice cartservice checkoutservice currencyservice emailservice frontend loadgenerator paymentservice productcatalogservice recommendationservice shippingservice)
   for CLUSTER in $CLUSTER_1 $CLUSTER_2 $CLUSTER_3; do
     echo -e "\nChecking deployments in $CLUSTER cluster..."
     for SVC in "${SERVICES[@]}"; do
       kubectl --context=${CLUSTER} -n online-boutique wait --for=condition=available --timeout=5m deployment $SVC
     done
   done

   # Checking redis-cart DB in Cluster_1 only
   kubectl --context=${CLUSTER_1} -n online-boutique wait --for=condition=available --timeout=5m deployment redis-cart
   ```

   The output is similar to the following:

   ```
   deployment.apps/frontend condition met
   ```

### Verifying Whereami

1. Verify that all Pods are running in all clusters.

   ```bash
   unset SERVICES
   SERVICES=(whereami-frontend whereami-backend)
   for CLUSTER in $CLUSTER_1 $CLUSTER_2 $CLUSTER_3; do
     echo -e "\nChecking deployments in $CLUSTER cluster..."
     for SVC in "${SERVICES[@]}"; do
       kubectl --context=${CLUSTER} -n whereami wait --for=condition=available --timeout=5m deployment $SVC
     done
   done
   ```

   The output is similar to the following:

   ```
   deployment.apps/whereami-frontend condition met
   deployment.apps/whereami-backend condition met
   ```

## Verifying multicluster service discovery and routing (east-west connectivity)

In this section, you verify that the three applciation clusters can discover services and route traffic to and from one another. You use the `whereami` application for this. Make sure that the whereami Pods from each cluster can discover and access Pods of other clusters.

1. Run the following script. This script takes a whereami Pod from every cluster and curls the whereami Service until it gets responses from all clusters.

   ```bash
   cat <<'EOF' > ${WORKDIR}/asm-multicluster-connectivity.sh
   #!/usr/bin/env bash

   CLUSTER_1_ZONE=us-west2-a
   CLUSTER_2_ZONE=us-central1-a
   CLUSTER_3_ZONE=us-central1-b

   for CLUSTER in ${CLUSTER_1} ${CLUSTER_2} ${CLUSTER_3}
   do
     echo -e "\e[1;92mTesting connectivity from $CLUSTER to all other clusters...\e[0m"
     echo -e "\e[96mDeploying curl utility in $CLUSTER cluster...\e[0m"
     kubectl --context=${CLUSTER} -n whereami apply -f https://raw.githubusercontent.com/istio/istio/master/samples/sleep/sleep.yaml
     kubectl --context=${CLUSTER} -n whereami wait --for=condition=available deployment sleep --timeout=5m
     for CLUSTER_ZONE in ${CLUSTER_1_ZONE} ${CLUSTER_2_ZONE} ${CLUSTER_3_ZONE}
     do
       echo -e "\e[92mTesting connectivity from $CLUSTER to $CLUSTER_ZONE...\e[0m"
       SLEEP_POD=`kubectl --context=${CLUSTER} -n whereami get pod -l app=sleep  -o jsonpath='{.items[0].metadata.name}'`
       ZONE=location
       while [ "$ZONE" != "$CLUSTER_ZONE" ]
       do
         ZONE=`kubectl --context=${CLUSTER} -n whereami exec -i -n whereami -c sleep $SLEEP_POD -- curl -s whereami-frontend.whereami:80 | jq -r '.zone'`
       done
       echo -e "\e[96m$CLUSTER can access $CLUSTER_ZONE\e[0m"
     done
     echo -e "\n"
   done
   EOF

   chmod +x ${WORKDIR}/asm-multicluster-connectivity.sh
   ${WORKDIR}/asm-multicluster-connectivity.sh
   ```

   The output is similar to the following:

   ```
   ...
   Testing connectivity from gke-central-1 to all other clusters...
   Deploying curl utility in gke-central-1 cluster...
   serviceaccount/sleep unchanged
   service/sleep unchanged
   deployment.apps/sleep unchanged
   deployment.apps/sleep condition met
   Testing connectivity from gke-central-1 to us-west2-a...
   gke-central-1 can access us-west2-a
   Testing connectivity from gke-central-1 to us-central1-a...
   gke-central-1 can access us-central1-a
   Testing connectivity from gke-central-1 to us-central1-b...
   gke-central-1 can access us-central1-b
   ...
   ```

### Recapping progress

You now have three GKE clusters with Anthos Service Mesh deployed using managed control plane (MCP). All three clusters are part of a single logical mesh. This means Services with the same name and in the same namespace are considered to be the same logical service. Running these logical services in multiple clusters are also called Distributed Services. YOu also deployed three applications (in three namespaces) as distributed services. You also verified that Services running in all three clusters can communicate between each other (east west traffic).

In the next sections, you focus on ingress or north-south traffic.

## Ingress and Multicluster Ingress

Ingress is a method of delivering traffic from an external client (in this case external to the service mesh) to a service (or a set of services) running inside the mesh. The teram "external client" refers to any client (or service) that is not part of the mesh. Inside the mesh, mesh takes care of service discovery and routing (see east-west connectivity section above). Clients and services running outside of the mesh need a way to securely access services running in the mesh.

```mermaid
graph TD;
subgraph GCP
  subgraph GKE[GKE cluster]
    subgraph ingns[ingress namespace]
      ing[ingress gateway]
    end
    subgraph appns[application namespace]
      app[Service]
    end
  end
end
Client ===> ing ===> app
```

Multicluster Ingress is a method of sending client traffic to a distributed service (a Kubernetes service that runs on multiple clusters). This design typically consists of a Google managed load balancer (an external GCLB or an internal ILB) as shown below.

```mermaid
graph TD;
subgraph GCP
  subgraph AGKE[GKE-1 cluster]
    subgraph Aingns[ingress namespace]
      Aing[ingress gateway]
    end
    subgraph Aappns[application namespace]
      Aapp[Service]
    end
  end
  subgraph BGKE[GKE-2 cluster]
    subgraph Bingns[ingress namespace]
      Bing[ingress gateway]
    end
    subgraph Bappns[application namespace]
      Bapp[Service]
    end
  end

end
Client ===> GLB ===> Aing & Bing
Aing ===> Aapp
Bing ===> Bapp
```

Clients access the Google managed load balancer, which sends traffic to multiple `ingress gateways` inside the mesh. In this case, you use Anthos Service Mesh ingress gateways. ASM ingress gateways can then utilize mTLS to send traffic to the desired backend Service. Since east-west traffic is already working in the mesh, ASM ingress gateways can send traffic "sideways" (or in the east-west direction) to healthy instances of Pods in other clusters if needed.

```mermaid
graph TD;
subgraph GCP
  subgraph AGKE[GKE-1 cluster]
    subgraph Aingns[ingress namespace]
      Aing[ingress gateway]
    end
    subgraph Aappns[application namespace]
      Aapp[Service]
    end
  end
  subgraph BGKE[GKE-2 cluster]
    subgraph Bingns[ingress namespace]
      Bing[ingress gateway]
    end
    subgraph Bappns[application namespace]
      Bapp[Service]
    end
  end

end
Client ===> GLB ===> Aing & Bing
Aing ===> Aapp
Aing -.-> Bapp
Bing ===> Bapp
Bing -.-> Aapp
```

You can use multiple ingress gateways. For example, a common scenario is using an external ingress for external (non-corp) clients and an internal ingress for all internal clients and services. Although you can use a single ingress for both internal and external clients, however, it is recommended that you use separate gateways. Using separate gateways gives you a more secure posture as well as makes it easy to manage configuration and certificates.

```mermaid
graph TD;
subgraph GCP
  subgraph AGKE[GKE-1 cluster]
    subgraph Aingns[ingress namespace]
      Aingx[external ingress gateway]
      Aingi[internal ingress gateway]
    end
    subgraph Aappns[application namespace]
      Aapp[Service]
    end
  end
  subgraph BGKE[GKE-2 cluster]
    subgraph Bingns[ingress namespace]
      Bingx[external ingress gateway]
      Bingi[internal ingress gateway]
    end
    subgraph Bappns[application namespace]
      Bapp[Service]
    end
  end

end
Client[external clients] ===> GLB ===> Aingx & Bingx
clienti[internal clients] ---> ILB ---> Aingi & Bingi
Aingx ===> Aapp
Aingx -.-> Bapp
Bingx ===> Bapp
Bingx -.-> Aapp
Aingi ---> Aapp
Aingi -.-> Bapp
Bingi ---> Bapp
Bingi -.-> Aapp
```

### Importance of location and locality load balancing

You can see in the diagram above that with two clusters, with two ingress gateways and a single service, there can be a lot of cross-cluster traffic. This means that with multicluster ingress, you must also control traffic flows. Typically this is done by location. For example, a client that is closer to `region1` should ideally access services in GKE clusters running in region1. Only if there are no healthy instances of the service in region1 should traffic flow to the next closest region. Similar logic can be apoplied at the zone or even at a cluster level. This design pattern is achieved using [locality load balancing](https://istio.io/latest/docs/tasks/traffic-management/locality-load-balancing/). The key proposition of locality load balancing is to always send traffic to the closest healthy endpoint of a service (whether its in the same cluster, zone or region and then other regions). When running globally distributed services, this concept is paramount.

In the following sections, you configure the two types of multi cluster ingress load balancing - external and internal. And then you configure locality load balancing for a service that is running in multiple clusters, in multiple zones and in multiple regions i.e. the `whereami` service.

## Configuring multicluster ingress with external load balancer (GCLB)

In this section, you configure multicluster ingress using a Google managed external HTTP load balancer (GCLB). If you are interested in configuring multicluster ingress using a Google managed internal HTTP load balancer (ILB), please skip to the section titled [Configuring multicluster ingress with internal load balancer (GCLB)](<#Configuring-multicluster-ingress-with-internal-load-balancer-(ILB)>).

Ensure that you have performed all of the steps prior to this section. You should have four GKE clusters. Three clusters should have ASM installed and multiple applications. You use the fourth cluster to configure multicluster ingress using the [Gateway API](https://cloud.google.com/kubernetes-engine/docs/concepts/gateway-api). Both internal and external ingress gateways can be deployed together or independently.

### Deploying ASM ingress gateway for external load balancing

1. Deploy ASM ingress gateway in all clusters. This gateway will strictly be used for external clients.

   ```bash
   cat <<EOF > ${WORKDIR}/asm-ingressgateway-external.yaml
   apiVersion: v1
   kind: Namespace
   metadata:
     name: asm-gateways
     labels:
       istio.io/rev: asm-managed
   ---
   apiVersion: v1
   kind: Service
   metadata:
     name: asm-ingressgateway-xlb
     namespace: asm-gateways
   spec:
     type: ClusterIP
     selector:
       asm: ingressgateway-xlb
     ports:
     - port: 80
       name: http
     - port: 443
       name: https
   ---
   apiVersion: apps/v1
   kind: Deployment
   metadata:
     name: asm-ingressgateway-xlb
     namespace: asm-gateways
   spec:
     selector:
       matchLabels:
         asm: ingressgateway-xlb
     template:
       metadata:
         annotations:
           # This is required to tell Anthos Service Mesh to inject the gateway with the
           # required configuration.
           inject.istio.io/templates: gateway
         labels:
           asm: ingressgateway-xlb
           # asm.io/rev: ${ASM_LABEL} # This is required only if the namespace is not labeled.
       spec:
         containers:
         - name: istio-proxy
           image: auto # The image will automatically update each time the pod starts.
   ---
   apiVersion: rbac.authorization.k8s.io/v1
   kind: Role
   metadata:
     name: asm-ingressgateway-sds
     namespace: asm-gateways
   rules:
   - apiGroups: [""]
     resources: ["secrets"]
     verbs: ["get", "watch", "list"]
   ---
   apiVersion: rbac.authorization.k8s.io/v1
   kind: RoleBinding
   metadata:
     name: asm-ingressgateway-sds
     namespace: asm-gateways
   roleRef:
     apiGroup: rbac.authorization.k8s.io
     kind: Role
     name: asm-ingressgateway-sds
   subjects:
     - kind: ServiceAccount
       name: default
   EOF

   kubectl --context=${CLUSTER_1} apply -f ${WORKDIR}/asm-ingressgateway-external.yaml
   kubectl --context=${CLUSTER_2} apply -f ${WORKDIR}/asm-ingressgateway-external.yaml
   kubectl --context=${CLUSTER_3} apply -f ${WORKDIR}/asm-ingressgateway-external.yaml
   ```

1. Verify that the ASM ingress gateway is deployed and running.

   ```bash
   kubectl --context=${CLUSTER_1} -n asm-gateways wait --for=condition=available deployment asm-ingressgateway-xlb --timeout=5m
   kubectl --context=${CLUSTER_2} -n asm-gateways wait --for=condition=available deployment asm-ingressgateway-xlb --timeout=5m
   kubectl --context=${CLUSTER_3} -n asm-gateways wait --for=condition=available deployment asm-ingressgateway-xlb --timeout=5m
   ```

   The output is similar to the following:

   ```
   deployment.apps/asm-ingressgateway-xlb condition met
   ```

### End to end multicluster ingress encryption

There are three legs of this ingress:

1. The first leg is from the client to the Google load balancer (GCLB). This leg uses the Google managed certificate or any certificate that is trusted by external clients.
2. The second leg is from the Google load balancer to the ASM ingress gateway. You can use any certificate between GCLB and ASM ingress gateway. In this tutorial you create a self-signed certificate. In production environments, you can use any PKI for this certificate.
3. The third leg is from the ASM ingress gateway to the desired Service. Traffic between ASM ingress gateways and all mesh services can be encrypted using mTLS. Mesh CA is the certificate authority that performs worload certificate management.

```mermaid
graph LR;
client --->|1. Google managed certificate| GCLB --->|2. Any SSL certificate| ASM[ASM Ingress gateways in multiple clusters] --->|3. mesh mTLS| Service[Distributed Services running in multiple clusters]
```

### Configure DNS and Google managed certificates

In this tutorial you use Google managed certificates so that clients can use public cert to access applications. [Google managed certificates](https://cloud.google.com/load-balancing/docs/ssl-certificates/google-managed-certs) is a Google managed service that allows you to create certificates that can be used with Google load balancers. You can use your own certificates instead of Google managed certificates.

1. In order to create a Google managed certificate, the IP address pointing to the DNS record must be reachable. Create a global static IP for GCLB (which will be used as the DNS record).

   ```bash
   gcloud compute addresses create gclb-ip --global
   export GCLB_IP=$(gcloud compute addresses describe gclb-ip --global --format=json | jq -r '.address')
   echo -e "GCLB_IP is ${GCLB_IP}"
   ```

1. Create free DNS names in the `cloud.goog` domain using Cloud Endpoints DNS service for Bank of Anthos, Online Boutique and the whereami applications. Learn more about [configuring DNS on the cloud.goog domain](https://cloud.google.com/endpoints/docs/openapi/cloud-goog-dns-configure).

   > You can choose your own custom domain as well. If you wish to do so, you can skip the following step. Ensure that you have three A records for the three applications (for example bank.yourdomain.com, shop.yourdomain.com and whereami.yourdomain.com). The A records should all point to the $GCLB_IP address (see previous step for $GCLB_IP).

   ```bash
   cat <<EOF > ${WORKDIR}/bank-openapi.yaml
   swagger: "2.0"
   info:
     description: "Cloud Endpoints DNS"
     title: "Cloud Endpoints DNS"
     version: "1.0.0"
   paths: {}
   host: "${BANK_DNS_PREFIX}.endpoints.${PROJECT_ID}.cloud.goog"
   x-google-endpoints:
   - name: "${BANK_DNS_PREFIX}.endpoints.${PROJECT_ID}.cloud.goog"
     target: "${GCLB_IP}"
   EOF

   cat <<EOF > ${WORKDIR}/shop-openapi.yaml
   swagger: "2.0"
   info:
     description: "Cloud Endpoints DNS"
     title: "Cloud Endpoints DNS"
     version: "1.0.0"
   paths: {}
   host: "${SHOP_DNS_PREFIX}.endpoints.${PROJECT_ID}.cloud.goog"
   x-google-endpoints:
   - name: "${SHOP_DNS_PREFIX}.endpoints.${PROJECT_ID}.cloud.goog"
     target: "${GCLB_IP}"
   EOF

   cat <<EOF > ${WORKDIR}/whereami-openapi.yaml
   swagger: "2.0"
   info:
     description: "Cloud Endpoints DNS"
     title: "Cloud Endpoints DNS"
     version: "1.0.0"
   paths: {}
   host: "${WHEREAMI_DNS_PREFIX}.endpoints.${PROJECT_ID}.cloud.goog"
   x-google-endpoints:
   - name: "${WHEREAMI_DNS_PREFIX}.endpoints.${PROJECT_ID}.cloud.goog"
     target: "${GCLB_IP}"
   EOF

   gcloud endpoints services deploy ${WORKDIR}/bank-openapi.yaml
   gcloud endpoints services deploy ${WORKDIR}/shop-openapi.yaml
   gcloud endpoints services deploy ${WORKDIR}/whereami-openapi.yaml
   ```

   > This step takes a few minutes to complete.

1. Create a Google managed cert for the three domains.

   > If you used your own custom domain, change the domains in the spec to point to your DNS names.

   ```bash
   cat <<EOF > ${WORKDIR}/managed-certs.yaml
   apiVersion: networking.gke.io/v1beta2
   kind: ManagedCertificate
   metadata:
     name: bank-managed-cert
     namespace: istio-system
   spec:
     domains:
     - "${BANK_DNS_PREFIX}.endpoints.${PROJECT_ID}.cloud.goog"
   ---
   apiVersion: networking.gke.io/v1beta2
   kind: ManagedCertificate
   metadata:
     name: shop-managed-cert
     namespace: istio-system
   spec:
     domains:
     - "${SHOP_DNS_PREFIX}.endpoints.${PROJECT_ID}.cloud.goog"
   ---
   apiVersion: networking.gke.io/v1beta2
   kind: ManagedCertificate
   metadata:
     name: whereami-managed-cert
     namespace: istio-system
   spec:
     domains:
     - "${WHEREAMI_DNS_PREFIX}.endpoints.${PROJECT_ID}.cloud.goog"
   EOF

   kubectl --context ${CLUSTER_CONFIG} create namespace istio-system

   kubectl --context ${CLUSTER_CONFIG} apply -f ${WORKDIR}/managed-certs.yaml
   ```

   Here we are creating the first leg of the ingress.

```mermaid
graph LR;
Adns[bank-of-anthos Cloud Endpoint DNS record] -.->|bank.endpoints.PROJECT_ID.cloud.goog resolving to GCLB_IP using Google managed cert| GCLB[GCLB frontend IP address]
Bdns[online-boutique Cloud Endpoint DNS record] -.->|shop.endpoints.PROJECT_ID.cloud.goog resolving to GCLB_IP using Google managed cert| GCLB
Cdns[whereami Cloud Endpoint DNS record] -.->|whereami.endpoints.PROJECT_ID.cloud.goog resolving to GCLB_IP using Google managed cert| GCLB
```

1. You can view the status of your certificates by describing these resources. The `spec` and `status` fields show configured data.

   ```bash
   kubectl --context ${CLUSTER_CONFIG} -n istio-system describe managedcertificate bank-managed-cert
   ```

   The output is similar to the following:

   ```
   Spec:
   Domains:
       boa1.endpoints.asm-multi-environment-cluster.cloud.goog
   Status:
   Certificate Name:    mcrt-f90cd39f-c706-4e51-9835-8493833ec2f8
   Certificate Status:  Provisioning
   Domain Status:
       Domain:  boa1.endpoints.asm-multi-environment-cluster.cloud.goog
       Status:  Provisioning
   ```

1. Get the certificate resource names. Ensure that there are values in each variable. If you do not see any value, re-run the following commands until you do see values.

   ```bash
   export BANK_MANAGED_CERT=$(kubectl --context ${CLUSTER_CONFIG} -n istio-system get managedcertificate bank-managed-cert -ojsonpath='{.status.certificateName}')
   export SHOP_MANAGED_CERT=$(kubectl --context ${CLUSTER_CONFIG} -n istio-system get managedcertificate shop-managed-cert -ojsonpath='{.status.certificateName}')
   export WHEREAMI_MANAGED_CERT=$(kubectl --context ${CLUSTER_CONFIG} -n istio-system get managedcertificate whereami-managed-cert -ojsonpath='{.status.certificateName}')

   echo -e "${BANK_MANAGED_CERT} \n${SHOP_MANAGED_CERT} \n${WHEREAMI_MANAGED_CERT}"
   ```

   The output is similar to the following. You must have all three values for the three certificates:

   ```
   mcrt-7d05eeec-eab3-47be-99fe-c8fdc2fe3a33
   mcrt-2bfe01d5-b4bd-4f32-be97-dc2ac25969db
   mcrt-3fd7dea6-c44b-49ec-ad5f-e6ad5908923a
   ```

   > It can take up to 30 minutes for the certificate to provision. You can proceed with the following steps.

### Configuring Cloud Armor policies

1. Create Cloud Armor policies. [Google Cloud Armor](https://cloud.google.com/armor) provides DDoS defense and [customizable security policies](https://cloud.google.com/armor/docs/configure-security-policies) that you can attach to a load balancer through Ingress resources. In the following steps, you create a security policy that uses [preconfigured rules](https://cloud.google.com/armor/docs/rule-tuning#preconfigured_rules) to block cross-site scripting (XSS) attacks.

   ```bash
   gcloud compute security-policies create gclb-fw-policy \
   --description "Block XSS attacks"

   gcloud compute security-policies rules create 1000 \
   --security-policy gclb-fw-policy \
   --expression "evaluatePreconfiguredExpr('xss-stable')" \
   --action "deny-403" \
   --description "XSS attack filtering"
   ```

### Configuring certificate for GCLB to ASM ingress gateway

1. Create & install self-signed certificate for the ingress gateways on both clusters.

   ```bash
   openssl req -new -newkey rsa:4096 -days 365 -nodes -x509 \
   -subj "/CN=frontend.endpoints.${PROJECT_ID}.cloud.goog/O=Edge2Mesh Inc" \
   -keyout frontend.endpoints.${PROJECT_ID}.cloud.goog.key \
   -out frontend.endpoints.${PROJECT_ID}.cloud.goog.crt

   kubectl --context=${CLUSTER_1} -n asm-gateways create secret tls edge2mesh-credential \
   --key=frontend.endpoints.${PROJECT_ID}.cloud.goog.key \
   --cert=frontend.endpoints.${PROJECT_ID}.cloud.goog.crt

   kubectl --context=${CLUSTER_2} -n asm-gateways create secret tls edge2mesh-credential \
   --key=frontend.endpoints.${PROJECT_ID}.cloud.goog.key \
   --cert=frontend.endpoints.${PROJECT_ID}.cloud.goog.crt

   kubectl --context=${CLUSTER_3} -n asm-gateways create secret tls edge2mesh-credential \
   --key=frontend.endpoints.${PROJECT_ID}.cloud.goog.key \
   --cert=frontend.endpoints.${PROJECT_ID}.cloud.goog.crt
   ```

   > Note: The `CN` and `subjectAlternativeName` in the certificate do not matter. GCLB accepts any certificate. Learn more about encryptring traffic between GCLB and backends using SSL certificates [here](https://cloud.google.com/load-balancing/docs/ssl-certificates/encryption-to-the-backends#secure_backend_protocol_considerations).

   This is creating the certificate for the second leg of the ingress.

   ```mermaid
   graph LR;
   Adns[bank-of-anthos Cloud Endpoint DNS record] -.->|bank.endpoints.PROJECT_ID.cloud.goog resolving to GCLB_IP using Google managed cert| GCLB[GCLB frontend IP address]
   Bdns[online-boutique Cloud Endpoint DNS record] -.->|shop.endpoints.PROJECT_ID.cloud.goog resolving to GCLB_IP using Google managed cert| GCLB
   Cdns[whereami Cloud Endpoint DNS record] -.->|whereami.endpoints.PROJECT_ID.cloud.goog resolving to GCLB_IP using Google managed cert| GCLB
   GCLB -.->|self-signed cert used to excrypt traffic between GCLB and ASM ingress gateways| ASM[ASM ingress gateways]
   ```

### Configuring Multicluster ingress resources

1. Create and apply `backendConfig`, `MultiClusterIngress` and `MultiClusterService` resources in the `gke-config` cluster.

   ```bash
   cat <<EOF > ${WORKDIR}/mci.yaml
   apiVersion: networking.gke.io/v1beta1
   kind: MultiClusterIngress
   metadata:
     name: asm-ingressgateway-xlb-multicluster-ingress
     namespace: asm-gateways
     annotations:
       networking.gke.io/static-ip: "${GCLB_IP}"
       networking.gke.io/pre-shared-certs: "${BANK_MANAGED_CERT},${SHOP_MANAGED_CERT},${WHEREAMI_MANAGED_CERT}"
   spec:
     template:
       spec:
         backend:
           serviceName: asm-ingressgateway-xlb-multicluster-svc
           servicePort: 443
   EOF

   cat <<EOF > ${WORKDIR}/mcs.yaml
   apiVersion: networking.gke.io/v1beta1
   kind: MultiClusterService
   metadata:
     name: asm-ingressgateway-xlb-multicluster-svc
     namespace: asm-gateways
     annotations:
       beta.cloud.google.com/backend-config: '{"ports": {"443":"asm-ingress-xlb-config"}}'
       networking.gke.io/app-protocols: '{"http2":"HTTP2"}'
   spec:
     template:
       spec:
         selector:
           asm: ingressgateway-xlb
         ports:
         - name: http2
           protocol: TCP
           port: 443 # Port the Service listens on
     clusters:
     - link: "${CLUSTER_1_ZONE}/${CLUSTER_1}"
     - link: "${CLUSTER_2_ZONE}/${CLUSTER_2}"
   EOF

   cat <<EOF > ${WORKDIR}/backend-config.yaml
   apiVersion: cloud.google.com/v1
   kind: BackendConfig
   metadata:
     name: asm-ingress-xlb-config
     namespace: asm-gateways
   spec:
     healthCheck:
       type: HTTP
       port: 15021
       requestPath: /healthz/ready
     securityPolicy:
       name: "gclb-fw-policy"
   EOF

   kubectl --context ${CLUSTER_CONFIG} create namespace asm-gateways

   kubectl --context ${CLUSTER_CONFIG} apply -f ${WORKDIR}/backend-config.yaml
   kubectl --context ${CLUSTER_CONFIG} apply -f ${WORKDIR}/mcs.yaml
   kubectl --context ${CLUSTER_CONFIG} apply -f ${WORKDIR}/mci.yaml
   ```

   > Note: if the ingress cluster GKE version is below 1.21, run the following command:

   ```bash
   kubectl --context ${CLUSTER_CONFIG} patch crd backendconfigs.cloud.google.com --type='json' -p='[{"op": "replace", "path": "/spec/versions/1/schema/openAPIV3Schema/properties/spec/properties/securityPolicy", "value":{"properties": {"name": {"type": "string"}}, "required": ["name" ],"type": "object"}}]'
   ```

### Configuring ASM Gateway and VirtualServices for external load balancing

1. Create a `Gateway` resource in the `asm-gateways` namespace. Creating this in the `asm-gateways` namespace allows you to use it in other namespaces (see `VirtualService` below). Gateways are generally owned by the platform admins or network admins team. Therefore, the `Gateway` resource is created in the `asm-gateways` namespace owned by the platform admin.

   ```bash
   cat <<EOF > ${WORKDIR}/asm-ingress-gateway.yaml
   apiVersion: networking.istio.io/v1alpha3
   kind: Gateway
   metadata:
     name: asm-ingress-gateway-xlb
     namespace: asm-gateways
   spec:
     selector:
       asm: ingressgateway-xlb # use ASM external ingress gateway
     servers:
     - port:
         number: 443
         name: https
         protocol: HTTPS
       hosts:
       - '*' # IMPORTANT: Must use wildcard here when using SSL, see note below
       tls:
         mode: SIMPLE
         credentialName: edge2mesh-credential
   EOF

   kubectl --context=${CLUSTER_1} apply -f ${WORKDIR}/asm-ingress-gateway.yaml
   kubectl --context=${CLUSTER_2} apply -f ${WORKDIR}/asm-ingress-gateway.yaml
   kubectl --context=${CLUSTER_3} apply -f ${WORKDIR}/asm-ingress-gateway.yaml
   ```

   > Note: You must use the wildcard `*` entry in the `hosts` field in the Gateway. GCLB does not use SNI extension to the backends. Using the wildcard entry sends the encrypted packet (from GCLB) to the ASM ingress gateway. ASM Ingress gateway decrypts the packet and uses the HTTP Host Header (in the decrypted packet) to make routing decisions. These decisions are based on `VirtualService` entries discussed next. Learn more about encryptring traffic between GCLB and backends using SSL certificates [here](https://cloud.google.com/load-balancing/docs/ssl-certificates/encryption-to-the-backends#secure_backend_protocol_considerations).

1. Create `VirtualServices` for the `bank`, `shop`, and `whereami` applications.

   ```bash
   cat <<EOF > ${WORKDIR}/bank-virtualservice.yaml
   apiVersion: networking.istio.io/v1alpha3
   kind: VirtualService
   metadata:
     name: bank-frontend-virtualservice-external
     namespace: bank-of-anthos
   spec:
     hosts:
     - "${BANK_DNS_PREFIX}.endpoints.${PROJECT_ID}.cloud.goog"
     gateways:
     - asm-gateways/asm-ingress-gateway-xlb
     http:
       - route:
         - destination:
             host: frontend
             port:
               number: 80
   EOF

   kubectl --context=${CLUSTER_1} apply -f ${WORKDIR}/bank-virtualservice.yaml
   kubectl --context=${CLUSTER_2} apply -f ${WORKDIR}/bank-virtualservice.yaml
   kubectl --context=${CLUSTER_3} apply -f ${WORKDIR}/bank-virtualservice.yaml

   cat <<EOF > ${WORKDIR}/shop-virtualservice.yaml
   apiVersion: networking.istio.io/v1alpha3
   kind: VirtualService
   metadata:
       name: shop-frontend-virtualservice-external
       namespace: online-boutique
   spec:
       hosts:
       - "${SHOP_DNS_PREFIX}.endpoints.${PROJECT_ID}.cloud.goog"
       gateways:
       - asm-gateways/asm-ingress-gateway-xlb
       http:
       - route:
           - destination:
               host: frontend
               port:
                 number: 80
   EOF

   kubectl --context=${CLUSTER_1} apply -f ${WORKDIR}/shop-virtualservice.yaml
   kubectl --context=${CLUSTER_2} apply -f ${WORKDIR}/shop-virtualservice.yaml
   kubectl --context=${CLUSTER_3} apply -f ${WORKDIR}/shop-virtualservice.yaml

   cat <<EOF > ${WORKDIR}/whereami-virtualservice.yaml
   apiVersion: networking.istio.io/v1alpha3
   kind: VirtualService
   metadata:
       name: whereami-frontend-virtualservice-external
       namespace: whereami
   spec:
       hosts:
       - "${WHEREAMI_DNS_PREFIX}.endpoints.${PROJECT_ID}.cloud.goog"
       gateways:
       - asm-gateways/asm-ingress-gateway-xlb
       http:
       - route:
           - destination:
               host: whereami-frontend
               port:
                 number: 80
   EOF

   kubectl --context=${CLUSTER_1} apply -f ${WORKDIR}/whereami-virtualservice.yaml
   kubectl --context=${CLUSTER_2} apply -f ${WORKDIR}/whereami-virtualservice.yaml
   kubectl --context=${CLUSTER_3} apply -f ${WORKDIR}/whereami-virtualservice.yaml
   ```

   Note that the `VirtualService` is created in the application namespace. Typically, the application owner decides and conifgures how and what traffic gets routed to the application so `VirtualService` is deployed by the app owner. You can also deploy the `VirtualServices` centrally, for example, in the `asm-gateways` namespace (owned by the platform admin). Then you can use the `exportTo` value to share the `VirtualService` to the application namespace. This way you can cerntalize all Ingress control centrally. The decision depends upon your organization and roles and responsibility structure.

### Accessing applications through the external GCLB

You can access all three applications once the SSL certificates have been provisioned.

1. Check the managed certificate status for all three certificates.

   ```bash
   kubectl --context ${CLUSTER_CONFIG} -n istio-system get managedcertificate bank-managed-cert -ojsonpath='{.status.certificateStatus}'
   kubectl --context ${CLUSTER_CONFIG} -n istio-system get managedcertificate shop-managed-cert -ojsonpath='{.status.certificateStatus}'
   kubectl --context ${CLUSTER_CONFIG} -n istio-system get managedcertificate whereami-managed-cert -ojsonpath='{.status.certificateStatus}'
   ```

   The output is similar to the following:

   ```
   # Bank managed cert
   Active

   # Shop managed cert
   Active

   # Simple managed cert
   Active
   ```

1. You can also check the certificate status via `gcloud`.

   ```bash
   gcloud beta compute ssl-certificates list --format='value(MANAGED_STATUS)'
   ```

   The output is similar to the following:

   ```
   ACTIVE
   ACTIVE
   ACTIVE
   ```

1. You can access the applications via the Cloud Endpoints DNS name.

   ```bash
   echo -e "https://${BANK_DNS_PREFIX}.endpoints.${PROJECT_ID}.cloud.goog"
   echo -e "https://${SHOP_DNS_PREFIX}.endpoints.${PROJECT_ID}.cloud.goog"
   echo -e "https://${WHEREAMI_DNS_PREFIX}.endpoints.${PROJECT_ID}.cloud.goog"
   ```

   Navigate through all three applications. They should be fully functional.

## Configuring multicluster ingress with internal load balancer (ILB)

In this section, you configure multicluster ingress using a Google managed internal HTTP load balancer (ILB). If you are interested in configuring multicluster ingress using a Google managed external HTTP load balancer (GCLB), please skip to the section titled [Configuring multicluster ingress with external load balancer (GCLB)](<#Configuring-multicluster-ingress-with-external-load-balancer-(GCLB)>). Both internal and external ingress gateways can be deployed together or independently.

Ensure that you have performed all of the steps prior to this section. You should have four GKE clusters. Three clusters should have ASM installed and multiple applications. You use the fourth cluster to configure multicluster ingress using the [Gateway API](https://cloud.google.com/kubernetes-engine/docs/concepts/gateway-api).

For multicluster ingress using ILB, you use the [Gateway API](https://cloud.google.com/kubernetes-engine/docs/concepts/gateway-api). When configuring multicluster ingress with external load balancer, you used the `MultiClusterIngress` and the `MultiClusterService` customr resources. These two resources allowed you to create a GCLB frontend with multiple backends (i.e ASM ingress gateways) in your GCP project. For multicluster ingress using ILB, you use the `Gateway` and the `HTTPRoute` resources. The end result is almost identical to the previous way.

### Configuring IAM permissions

1. Ensure that you have the required Gateway API resources present in the `gke-config` cluster.

   ```bash
   kubectl --context=${CLUSTER_CONFIG} get gatewayclasses
   ```

   The output is similar to the following:

   ```
   gke-l7-global-external-managed      networking.gke.io/gateway   True       94m
   gke-l7-global-external-managed-mc   networking.gke.io/gateway   True       79m
   gke-l7-gxlb                         networking.gke.io/gateway   True       94m
   gke-l7-gxlb-mc                      networking.gke.io/gateway   True       79m
   gke-l7-rilb                         networking.gke.io/gateway   True       94m
   gke-l7-rilb-mc                      networking.gke.io/gateway   True       79m
   ```

   > Note: Ensure that you have the `gke-l7-rilb-mc` available on the cluster. The notation means its an HTTP (`l7`), regional internal load balancer (`rilb`) multi cluster (`mc`) resource.

1. Grant the required Identity and Access Management (IAM) permissions required for MCS.

   ```bash
    gcloud projects add-iam-policy-binding ${PROJECT_ID} \
    --member "serviceAccount:${PROJECT_ID}.svc.id.goog[gke-mcs/gke-mcs-importer]" \
    --role "roles/compute.networkViewer" \
    --project=${PROJECT_ID}
   ```

1. Grant Identity and Access Management (IAM) permissions required by the Gateway controller.

   ```bash
   export PROJECT_NUM=$(gcloud projects describe ${PROJECT_ID} --format='value(projectNumber)')
   gcloud projects add-iam-policy-binding ${PROJECT_ID} \
   --member "serviceAccount:service-${PROJECT_NUM}@gcp-sa-multiclusteringress.iam.gserviceaccount.com" \
   --role "roles/container.admin" \
   --project=${PROJECT_ID}

   # The following role (Project Editor) will not be required in the future
   gcloud projects add-iam-policy-binding ${PROJECT_ID} \
   --member "serviceAccount:service-${PROJECT_NUM}@gcp-sa-multiclusteringress.iam.gserviceaccount.com" \
   --role "roles/editor" \
   --project=${PROJECT_ID}
   ```

### Creating a proxy-only subnet

1. Internal load balancers are Google managed proxies that utilize VPC resources. In order to configure ILBs, you must have a [proxy-only subnet](https://cloud.google.com/load-balancing/docs/l7-internal/proxy-only-subnets) configured in your VPC (in the appropriate region). Create a proxy-only subnet in the `us-central1` region.

   ```bash
   gcloud compute networks subnets create ${PROXY_ONLY_SUBNET} \
   --purpose=INTERNAL_HTTPS_LOAD_BALANCER \
   --role=ACTIVE \
   --region=${ILB_REGION} \
   --network=${VPC} \
   --range=${PROXY_ONLY_CIDR}
   ```

### Deploying ASM ingress gateway for internal load balancing

1. Deploy ASM internal ingress gateway in all clusters. This gateway will strictly be used for internal clients.

   ```bash
   cat <<EOF > ${WORKDIR}/asm-ingressgateway-internal.yaml
   apiVersion: v1
   kind: Namespace
   metadata:
     name: asm-gateways
     labels:
       istio.io/rev: asm-managed
   ---
   apiVersion: v1
   kind: Service
   metadata:
     name: asm-ingressgateway-ilb
     namespace: asm-gateways
   spec:
     type: ClusterIP
     selector:
       asm: ingressgateway-ilb
     ports:
     - port: 80
       name: http
     - port: 443
       name: https
   ---
   apiVersion: apps/v1
   kind: Deployment
   metadata:
     name: asm-ingressgateway-ilb
     namespace: asm-gateways
   spec:
     selector:
       matchLabels:
         asm: ingressgateway-ilb
     template:
       metadata:
         annotations:
           # This is required to tell Anthos Service Mesh to inject the gateway with the
           # required configuration.
           inject.istio.io/templates: gateway
         labels:
           asm: ingressgateway-ilb
           # asm.io/rev: ${ASM_LABEL} # This is required only if the namespace is not labeled.
       spec:
         containers:
         - name: istio-proxy
           image: auto # The image will automatically update each time the pod starts.
   ---
   apiVersion: rbac.authorization.k8s.io/v1
   kind: Role
   metadata:
     name: asm-ingressgateway-sds
     namespace: asm-gateways
   rules:
   - apiGroups: [""]
     resources: ["secrets"]
     verbs: ["get", "watch", "list"]
   ---
   apiVersion: rbac.authorization.k8s.io/v1
   kind: RoleBinding
   metadata:
     name: asm-ingressgateway-sds
     namespace: asm-gateways
   roleRef:
     apiGroup: rbac.authorization.k8s.io
     kind: Role
     name: asm-ingressgateway-sds
   subjects:
     - kind: ServiceAccount
       name: default
   EOF

   kubectl --context=${CLUSTER_1} apply -f ${WORKDIR}/asm-ingressgateway-internal.yaml
   kubectl --context=${CLUSTER_2} apply -f ${WORKDIR}/asm-ingressgateway-internal.yaml
   kubectl --context=${CLUSTER_3} apply -f ${WORKDIR}/asm-ingressgateway-internal.yaml
   ```

1. Verify that the ASM ingress gateway is deployed and running.

   ```bash
   kubectl --context=${CLUSTER_1} -n asm-gateways wait --for=condition=available deployment asm-ingressgateway-ilb --timeout=5m
   kubectl --context=${CLUSTER_2} -n asm-gateways wait --for=condition=available deployment asm-ingressgateway-ilb --timeout=5m
   kubectl --context=${CLUSTER_3} -n asm-gateways wait --for=condition=available deployment asm-ingressgateway-ilb --timeout=5m
   ```

   The output is similar to the following:

   ```
   deployment.apps/asm-ingressgateway-ilb condition met
   ```

### Creating ServiceExports

1. ILB uses the `asm-ingressgateway-ilb` Services in each cluster as backends. The `asm-ingressgateway-ilb` can then send traffic to the desired distributed service. In order for the ILB to use the `asm-ingressgateway-ilb` Services from all regional clusters, you must "export" the Services so that the `gke-config` cluster can use the "imported" services as backends. Create ServiceExport objects for the `asm-ingressgateway-ilb` Service from the regional clusters.

   ```bash
   cat <<EOF > ${WORKDIR}/asm-ingressgateway-ilb-export.yaml
   kind: ServiceExport
   apiVersion: net.gke.io/v1
   metadata:
     name: asm-ingressgateway-ilb
     namespace: asm-gateways
   EOF

   # You only need to deploy the ServiceExport in the us-central1 region which has the two clusters
   kubectl --context=${CLUSTER_2} apply -f ${WORKDIR}/asm-ingressgateway-ilb-export.yaml
   kubectl --context=${CLUSTER_3} apply -f ${WORKDIR}/asm-ingressgateway-ilb-export.yaml
   ```

1. Verify the `ServiceImport` in the `gke-config` cluster.

   ```bash
   kubectl --context=${CLUSTER_CONFIG} -n asm-gateways get serviceimports
   ```

   The output is similar to the following:

   ```
   NAME                     TYPE           IP                AGE
   asm-ingressgateway-ilb   ClusterSetIP   ["10.77.94.35"]   10m
   ```

   > This output may take a few minutes to show up.

   When a `ServiceImport` object is created, a derived Kubernetes Service is also created. This derived Service is there so that it can configure `Endpoints`. The Endpoints of the derived Service are the Pod IP addresses of the "exported" Services. In this case, that exported Service is the `asm-ingressgateway-ilb` Service from the two regional clusters.

1. Verify that the Endpoints have the correct Pod IP and Service ports.

   ```bash
   export DERIVED_SVC=$(kubectl --context=${CLUSTER_CONFIG} -n asm-gateways get serviceimports asm-ingressgateway-ilb -ojsonpath='{.metadata.annotations.net\.gke\.io\/derived-service}')
   echo -e "Derived Service is ${DERIVED_SVC}"

   kubectl --context=${CLUSTER_CONFIG} -n asm-gateways get endpoints ${DERIVED_SVC} -oyaml
   ```

   The output is similar to the following:

   ```
   subsets:
   - notReadyAddresses:
     - ip: 10.124.1.30
     ports:
     - name: http
       port: 80
       protocol: TCP
   - notReadyAddresses:
     - ip: 10.124.1.30
     ports:
     - name: https
       port: 443
       protocol: TCP
   - notReadyAddresses:
     - ip: 10.72.0.26
     ports:
     - name: https
       port: 443
       protocol: TCP
   - notReadyAddresses:
     - ip: 10.72.0.26
     ports:
     - name: http
       port: 80
       protocol: TCP
   ```

   Note that in this example there are two IP addresses, these IP addresses are the Pod IP addresses from the `asm-ingressgateway-ilb` Service in CLUSTER_2 and CLUSTER_3. Each IP address has two ports (80 and 443) which are the Ports from the `asm-ingressgateway-ilb` Service.

   > Note: It may take a few minutes until you see the derived service and endpoints fully populated. Ignore the `notReadyAddresses` title. This will change when you actually send traffic to these IP addresses later.

   Next, you create a `Gateway` resource in the `gke-config` cluster. This `Gateway` resource is different from the ASM/Istio `Gateway` resource (managed by the Kubernetes SIG-NETWORK community, see docs [here](https://gateway-api.sigs.k8s.io/)). This resource provisions load balancers in your environment. In this example, you provision an ILB using the `gke-l7-rilb-mc` GatewayClass. This is an internal regional HTTP load balancer with multicluster ingress.

### Creating Gateway and HTTPRoute resources for internal load balancing

1. Create a `Gateway` resource.

   ```bash
   cat <<EOF > ${WORKDIR}/gateway-internal-http-ilb.yaml
   apiVersion: gateway.networking.k8s.io/v1beta1
   kind: Gateway
   metadata:
     name: asm-ingressgateway-http-ilb
     namespace: asm-gateways
   spec:
     gatewayClassName: gke-l7-rilb-mc
     listeners:
     - name: web
       port: 80
       protocol: HTTP
       allowedRoutes:
         kinds: 
           - kind: HTTPRoute
   EOF

   kubectl --context=${CLUSTER_CONFIG} apply -f ${WORKDIR}/gateway-internal-http-ilb.yaml
   ```

1. Verify that the Gateway resources is created.

   ```bash
   kubectl get events --field-selector involvedObject.kind=Gateway,involvedObject.name=asm-ingressgateway-http-ilb --context=${CLUSTER_CONFIG} --namespace asm-gateways
   ```

   The output should be similar to the following:

   ```
   ...
   61s         Normal    SYNC     gateway/asm-ingressgateway-http-ilb   SYNC on asm-gateways/asm-ingressgateway-http-ilb was a success
   ```

   > Note: If you see errors about resources not ready, wait a few minutes and re-run the command above. The last entry should state that `SYNC` was a success.

1. Create an `HTTPRoute` resource. The `HTTPRoute` resource creates backends to the ILB created by the `Gateway` resource in the previous step. These backends are ServiceImports, or in this case the `asm-ingressgateway-ilb` Service.

   ```bash
   cat <<EOF > ${WORKDIR}/asm-ingressgateway-ilb-httproute.yaml
   apiVersion: gateway.networking.k8s.io/v1beta1
   kind: HTTPRoute
   metadata:
     name: asm-ingressgateway-route-ilb
     namespace: asm-gateways
   spec:
     parentRefs:
     - kind: Gateway
       name: asm-ingressgateway-http-ilb
       namespace: asm-gateways
     rules:
     - backendRefs:
       - group: net.gke.io
         kind: ServiceImport
         name: asm-ingressgateway-ilb
         port: 80
   EOF

   kubectl --context=${CLUSTER_CONFIG} apply -f ${WORKDIR}/asm-ingressgateway-ilb-httproute.yaml
   ```

1. Verify that the `HTTPRoute` resource is created successfully.

   ```bash
   kubectl get events --field-selector involvedObject.kind=HTTPRoute,involvedObject.name=asm-ingressgateway-route-ilb --context=${CLUSTER_CONFIG} --namespace asm-gateways
   ```

   The output should be similar to the following:

   ```
   LAST SEEN   TYPE     REASON   OBJECT                                   MESSAGE
   101s        Normal   ADD      httproute/asm-ingressgateway-route-ilb   asm-gateways/asm-ingressgateway-route-ilb
   101s        Normal   ADD      httproute/asm-ingressgateway-route-ilb   asm-gateways/asm-ingressgateway-route-ilb
   37s         Normal   SYNC     httproute/asm-ingressgateway-route-ilb   Bind of HTTPRoute "asm-gateways/asm-ingressgateway-route-ilb" to Gateway "asm-gateways/asm-ingressgateway-http-ilb" was a success
   37s         Normal   SYNC     httproute/asm-ingressgateway-route-ilb   Reconciliation of HTTPRoute "asm-gateways/asm-ingressgateway-route-ilb" bound to Gateway "asm-gateways/asm-ingressgateway-http-ilb" was a success
   ```

   > Note: The latest entry is at the bottom (confirmed by the time stamp in the first column).

### Creating ASM Gateway and VirtualServices for internal load balancing

1. Create an ASM `Gateway` and `VirtualService` resources for the `whereami-frontend` service in both application clusters.

   ```bash
   cat <<EOF > ${WORKDIR}/gateway-vs-whereami-frontend.yaml
   apiVersion: networking.istio.io/v1beta1
   kind: Gateway
   metadata:
     name: asm-ingress-gateway-ilb
     namespace: asm-gateways
   spec:
     selector:
       asm: ingressgateway-ilb # use the internal ASM ingress gateway
     servers:
     - port:
         number: 80
         name: http
         protocol: HTTP
       hosts:
       - '*'
   ---
   apiVersion: networking.istio.io/v1beta1
   kind: VirtualService
   metadata:
     name: whereami-virtualservice-ilb
     namespace: whereami
   spec:
     hosts:
     - "*"
     gateways:
     - asm-gateways/asm-ingress-gateway-ilb
     http:
       - route:
         - destination:
             host: whereami-frontend
             port:
               number: 80
   EOF

   kubectl --context=${CLUSTER_2} apply -f ${WORKDIR}/gateway-vs-whereami-frontend.yaml
   kubectl --context=${CLUSTER_3} apply -f ${WORKDIR}/gateway-vs-whereami-frontend.yaml
   ```

### Accessing applications through the ILB

1. Get the ILB IP address.

   ```bash
   ILB_IP=$(kubectl --context=${CLUSTER_CONFIG} -n asm-gateways get gateway asm-ingressgateway-http-ilb -o=jsonpath="{.status.addresses[0].value}")
   echo -e "ILB IP address is ${ILB_IP}"
   ```

1. Create a `sleep` (curl utility) Deployment in the `gke-config` cluster which is not part of the service mesh. And then curl to the `whereami-frontend` service through the ILB ingress. The `sleep` Pod simulates a local client that is not part of the mesh and therefore must use an ingress to access an application running inside the mesh.

   ```bash
   kubectl --context=${CLUSTER_CONFIG} -n default apply -f https://raw.githubusercontent.com/istio/istio/master/samples/sleep/sleep.yaml
   kubectl --context=${CLUSTER_CONFIG} -n default wait --for=condition=available deployment sleep --timeout=5m

   # Get the sleep pod
   export SLEEP_POD=$(kubectl --context=${CLUSTER_CONFIG} -n default get pod -l app=sleep  -o jsonpath='{.items[0].metadata.name}')

   kubectl --context=${CLUSTER_CONFIG} -n default exec -i -n default -c sleep ${SLEEP_POD} -- curl -s http://${ILB_IP}
   ```

   The output is similar to the following:

   ```
   {
     "backend_result": {
       "cluster_name": "gke-central-2",
       "host_header": "whereami-backend",
       "metadata": "backend",
       "node_name": "gke-gke-central-2-default-pool-a24995d0-5rlr",
       "pod_ip": "10.124.0.18",
       "pod_name": "whereami-backend-76d7d597d4-sbb2x",
       "pod_name_emoji": "🫰",
       "pod_namespace": "whereami",
       "pod_service_account": "whereami-ksa-backend",
       "project_id": "qwiklabs-gcp-00-1611377db6b6",
       "timestamp": "2021-10-13T22:38:24",
       "zone": "us-central1-b"
     },
     "cluster_name": "gke-west-1",
     "host_header": "10.128.0.7",
     "metadata": "frontend",
     "node_name": "gke-gke-west-1-default-pool-270d4f65-szhq",
     "pod_ip": "10.88.0.24",
     "pod_name": "whereami-frontend-9868ff95b-4gs46",
     "pod_name_emoji": "🧛🏼♂️",
     "pod_namespace": "whereami",
     "pod_service_account": "whereami-ksa-frontend",
     "project_id": "qwiklabs-gcp-00-1611377db6b6",
     "timestamp": "2021-10-13T22:38:24",
     "zone": "us-west2-a"
   }
   ```

### Understanding the whereami application

The `whereami` application is composed of two Kubernetes Services: a `whereami-frontend` Service which a client accesses and a `whereami-backend` Service which is accessed by the `whereami-frontend` Service. Both of these Services run in all application clusters (as Distributed Services) in the `whereami` namespace. This allows you to test both north-south (i.e. client to whereami-frontend) and east-west (i.e. whereami-frontend to whereami-backend) traffic in a single request flow.

We you curl the `whereami-frontend`, it responds with metadata information (in JSON format) about both the frontend and the backend Pods that are responding This metadata includes location, which GKE cluster the Pods are running in, Pod names etc. This allows you to easily tell how the end to end request flow is happening.

For example, if you run the following command:

```bash
curl "https://${WHEREAMI_DNS_PREFIX}.endpoints.${PROJECT_ID}.cloud.goog"
```

The response might be something like the following:

    {
    "backend_result": {
      "cluster_name": "gke-west-1",
      "host_header": "whereami-backend",
      "metadata": "backend",
      "node_name": "gke-gke-west-1-default-pool-4b0103e4-h0vc",
      "pod_ip": "10.88.1.21",
      "pod_name": "whereami-backend-76d7d597d4-t4qs9",
      "pod_name_emoji": "👩🏼🌾",
      "pod_namespace": "whereami",
      "pod_service_account": "whereami-ksa-backend",
      "project_id": "qwiklabs-gcp-01-13b829ba41ea",
      "timestamp": "2021-10-13T18:16:40",
      "zone": "us-west2-a"
    },
    "cluster_name": "gke-central-1",
    "host_header": "whereami.endpoints.qwiklabs-gcp-01-13b829ba41ea.cloud.goog",
    "metadata": "frontend",
    "node_name": "gke-gke-central-1-default-pool-7095287d-8jlt",
    "pod_ip": "10.72.1.23",
    "pod_name": "whereami-frontend-9868ff95b-4smxt",
    "pod_name_emoji": "🎎",
    "pod_namespace": "whereami",
    "pod_service_account": "whereami-ksa-frontend",
    "project_id": "qwiklabs-gcp-01-13b829ba41ea",
    "timestamp": "2021-10-13T18:13:20",
    "zone": "us-central1-a"

}

The top section with the object key of "backend_result" is showing you Pod metadata from the `whereami-backend` Pod. The bottom section (outside of the "backend_result" object) is showing you metadata form the `whereami-frontend` Pod. Since we have both (the whereami-frontend and where-backedn) Services deployed as Distributed Services to all clusters, you may want to know exactly which Pod (in which cluster) is responding.

In the example output above, you can see that the `whereami-frontend` is running in the `gke-central-1` cluster but the `whereami-backend` that responded is running in the `gke-west-1` cluster. This may or may not be the desired traffic pattern.

There are three locations to consider:

1. Client location
1. ASM Ingress location
1. Service location

Ideally you would like the clients to acccess the closest ASM ingress gateways and the ASM ingress gateways to access the closest Service location. This is where locality load balancing can help us control traffic flow based on location.

In the next section, you configure locality load balancing for the `whereami` application and test regular and failover scenarios.

## Setting up locality load balancing in your mesh

Locality load balancing allows you to control traffic flows based on the location on the client and the intended service. Locality load balancing is a mesh concept therefore it applies after the requests reach the ASM ingress gateway. From ASM ingressgateway downstream, you use locality load balancing to control the flow of traffic. In this section, you set up locality load balancing for the `whereami` application only. The same concepts can be applied for other Services.

1. Setup Locality based routing destination rules for the `whereami` virtual services.

   ```bash
   cat <<EOF > ${WORKDIR}/destinationrules-whereami-frontend-cluster-1.yaml
   apiVersion: networking.istio.io/v1beta1
   kind: DestinationRule
   metadata:
     name: whereami-frontend-destrule-cluster-1
     namespace: whereami
   spec:
     host: whereami-frontend.whereami.svc.cluster.local
     trafficPolicy:
       connectionPool:
         http:
           maxRequestsPerConnection: 1
       loadBalancer:
         simple: ROUND_ROBIN
         localityLbSetting:
           enabled: true
         # failover:
         #   - from: ${CLUSTER_2_REGION}
         #     to: ${CLUSTER_1_REGION}
           failoverPriority:
             - "topology.kubernetes.io/region"
             - "topology.kubernetes.io/zone"
             - "topology.kubernetes.io/cluster"
       outlierDetection:
         consecutive5xxErrors: 1
         interval: 1s
         baseEjectionTime: 1m
   EOF

   cat <<EOF > ${WORKDIR}/destinationrules-whereami-frontend-cluster-2.yaml
   apiVersion: networking.istio.io/v1beta1
   kind: DestinationRule
   metadata:
     name: whereami-frontend-destrule-cluster-2
     namespace: whereami
   spec:
     host: whereami-frontend.whereami.svc.cluster.local
     trafficPolicy:
       connectionPool:
         http:
           maxRequestsPerConnection: 1
       loadBalancer:
         simple: ROUND_ROBIN
         localityLbSetting:
           enabled: true
         # failover:
         #   - from: ${CLUSTER_2_REGION}
         #     to: ${CLUSTER_1_REGION}
           failoverPriority:
             - "topology.kubernetes.io/region"
             - "topology.kubernetes.io/zone"
             - "topology.kubernetes.io/cluster"
       outlierDetection:
         consecutive5xxErrors: 1
         interval: 1s
         baseEjectionTime: 1m
   EOF

   cat <<EOF > ${WORKDIR}/destinationrules-whereami-frontend-cluster-3.yaml
   apiVersion: networking.istio.io/v1beta1
   kind: DestinationRule
   metadata:
     name: whereami-frontend-destrule-cluster-3
     namespace: whereami
   spec:
     host: whereami-frontend.whereami.svc.cluster.local
     trafficPolicy:
       connectionPool:
         http:
           maxRequestsPerConnection: 1
       loadBalancer:
         simple: ROUND_ROBIN
         localityLbSetting:
           enabled: true
         # failover:
         #   - from: ${CLUSTER_2_REGION}
         #     to: ${CLUSTER_1_REGION}
           failoverPriority:
             - "topology.kubernetes.io/region"
             - "topology.kubernetes.io/zone"
             - "topology.kubernetes.io/cluster"
       outlierDetection:
         consecutive5xxErrors: 1
         interval: 1s
         baseEjectionTime: 1m
   EOF

   cat <<EOF > ${WORKDIR}/destinationrules-whereami-backend-cluster-1.yaml
   apiVersion: networking.istio.io/v1beta1
   kind: DestinationRule
   metadata:
     name: whereami-backend-destrule-cluster-1
     namespace: whereami
   spec:
     host: whereami-backend.whereami.svc.cluster.local
     trafficPolicy:
       connectionPool:
         http:
           maxRequestsPerConnection: 1
       loadBalancer:
         simple: ROUND_ROBIN
         localityLbSetting:
           enabled: true
         # failover:
         #   - from: ${CLUSTER_2_REGION}
         #     to: ${CLUSTER_1_REGION}
           failoverPriority:
             - "topology.kubernetes.io/region"
             - "topology.kubernetes.io/zone"
             - "topology.kubernetes.io/cluster"
       outlierDetection:
         consecutive5xxErrors: 1
         interval: 1s
         baseEjectionTime: 1m
   EOF

   cat <<EOF > ${WORKDIR}/destinationrules-whereami-backend-cluster-2.yaml
   apiVersion: networking.istio.io/v1beta1
   kind: DestinationRule
   metadata:
     name: whereami-backend-destrule-cluster-2
     namespace: whereami
   spec:
     host: whereami-backend.whereami.svc.cluster.local
     trafficPolicy:
       connectionPool:
         http:
           maxRequestsPerConnection: 1
       loadBalancer:
         simple: ROUND_ROBIN
         localityLbSetting:
           enabled: true
         # failover:
         #   - from: ${CLUSTER_2_REGION}
         #     to: ${CLUSTER_1_REGION}
           failoverPriority:
             - "topology.kubernetes.io/region"
             - "topology.kubernetes.io/zone"
             - "topology.kubernetes.io/cluster"
       outlierDetection:
         consecutive5xxErrors: 1
         interval: 1s
         baseEjectionTime: 1m
   EOF

   cat <<EOF > ${WORKDIR}/destinationrules-whereami-backend-cluster-3.yaml
   apiVersion: networking.istio.io/v1beta1
   kind: DestinationRule
   metadata:
     name: whereami-backend-destrule-cluster-3
     namespace: whereami
   spec:
     host: whereami-backend.whereami.svc.cluster.local
     trafficPolicy:
       connectionPool:
         http:
           maxRequestsPerConnection: 1
       loadBalancer:
         simple: ROUND_ROBIN
         localityLbSetting:
           enabled: true
         # failover:
         #   - from: ${CLUSTER_2_REGION}
         #     to: ${CLUSTER_1_REGION}
           failoverPriority:
             - "topology.kubernetes.io/region"
             - "topology.kubernetes.io/zone"
             - "topology.kubernetes.io/cluster"
       outlierDetection:
         consecutive5xxErrors: 1
         interval: 1s
         baseEjectionTime: 1m
   EOF

   kubectl --context=${CLUSTER_1} apply -f ${WORKDIR}/destinationrules-whereami-frontend-cluster-1.yaml
   kubectl --context=${CLUSTER_1} apply -f ${WORKDIR}/destinationrules-whereami-backend-cluster-1.yaml

   kubectl --context=${CLUSTER_2} apply -f ${WORKDIR}/destinationrules-whereami-frontend-cluster-2.yaml
   kubectl --context=${CLUSTER_2} apply -f ${WORKDIR}/destinationrules-whereami-backend-cluster-2.yaml

   kubectl --context=${CLUSTER_3} apply -f ${WORKDIR}/destinationrules-whereami-frontend-cluster-3.yaml
   kubectl --context=${CLUSTER_3} apply -f ${WORKDIR}/destinationrules-whereami-backend-cluster-3.yaml
   ```

   The `whereami` application now has locality load balancing enabled. This means that the requests will go to the "closest" healthiest endpoint to the client. If you are in (or close to) region1, your requests will always go to region1, as long as there is a healthy instance of the `whereami` application in region1. If region1 does not have a healthy instance and region2 does, then the requests will go to region2. The opposite is also true.

1. Verify that locality load balancing is working as expected.

   ```bash
   export ILB_IP=$(kubectl --context=${CLUSTER_CONFIG} -n asm-gateways get gateway asm-ingressgateway-http-ilb -o=jsonpath="{.status.addresses[0].value}")
   export SLEEP_POD=$(kubectl --context=${CLUSTER_CONFIG} -n default get pod -l app=sleep  -o jsonpath='{.items[0].metadata.name}')
   # Through the external load balancer
   bash << EOF
   echo -e "\nthru External LB"
   for n in {1..10}; do
       curl -s "https://${WHEREAMI_DNS_PREFIX}.endpoints.${PROJECT_ID}.cloud.goog" | jq -r ['.zone,.backend_result.zone] | @csv';
   done

   echo -e "\nthru Internal LB"
   for n in {1..10}; do
     kubectl --context=${CLUSTER_CONFIG} -n default exec -i -n default -c sleep ${SLEEP_POD} -- curl -s http://${ILB_IP} | jq -r ['.zone,.backend_result.zone] | @csv';
   done
   EOF
   ```

   The output is similar to the following (you may see a different zone based on your client location):

   ```
   # thru External LB
   "us-west2-a","us-west2-a"
   "us-west2-a","us-west2-a"
   "us-west2-a","us-west2-a"
   "us-west2-a","us-west2-a"
   "us-west2-a","us-west2-a"
   "us-west2-a","us-west2-a"
   "us-west2-a","us-west2-a"
   "us-west2-a","us-west2-a"
   "us-west2-a","us-west2-a"
   "us-west2-a","us-west2-a"

   # thru Internal LB
   "us-central1-a","us-central1-a"
   "us-central1-a","us-central1-a"
   "us-central1-b","us-central1-b"
   "us-central1-b","us-central1-b"
   "us-central1-b","us-central1-b"
   "us-central1-b","us-central1-b"
   "us-central1-a","us-central1-a"
   "us-central1-a","us-central1-a"
   "us-central1-a","us-central1-a"
   "us-central1-a","us-central1-a"
   ```

   The first value is the response from the `whereami-frontend` app and the second value is the response from the `whereami-backend` app. Depending upon which region (or zone) you are close to, you get responses from the closest zone. No matter how many times you access the `whereami` app, you get responses from the closest region/zone.

   There is only one cluster in the `us-west2` region in zone `us-west2-a`, therefore all responses are coming from the same zone. There are two clusters in the `us-central1` region, one is zone `us-central1-a` and one in zone `us-central1-b`. The ILB is sending requests to both `whereami-frontend` Services. The `whereami-frontend` service is sending request to its closest `whereami-backend` which in all cases is in the same cluster. Whenever the ILB sends requests to zone `us-central1-a`, the `whereami-backend` in the same zone responds.

### Testing failure scenarios with locality load balancing

1. Scale both `whereami` Deployments (frontend and backend) in one of the clusters in `us-central1` region. This simulates an outage or a scheudled maintenance event. ANd then perform the curl from the `sleep` Pod using the ILB again.

   ```bash
   kubectl --context=${CLUSTER_2} -n whereami scale deploy whereami-frontend --replicas=0
   kubectl --context=${CLUSTER_2} -n whereami scale deploy whereami-backend --replicas=0

   sleep 5 # Wait a few seconds until both Deployments scale down to 0
   bash << EOF
   for n in {1..10}; do
     kubectl --context=${CLUSTER_CONFIG} -n default exec -i -n default -c sleep ${SLEEP_POD} -- curl -s http://${ILB_IP} | jq -r ['.zone,.backend_result.zone] | @csv';
   done
   EOF
   ```

   The output should be similar to the following:

   ```
   "us-central1-b","us-central1-b"
   "us-central1-b","us-central1-b"
   "us-central1-b","us-central1-b"
   "us-central1-b","us-central1-b"
   "us-central1-b","us-central1-b"
   "us-central1-b","us-central1-b"
   "us-central1-b","us-central1-b"
   "us-central1-b","us-central1-b"
   "us-central1-b","us-central1-b"
   "us-central1-b","us-central1-b"
   ```

   Note that ILB detects that one of the backends (i.e. the `asm-ingressgateway-ilb`) is unhealthy but there is another healthy backend in the same region and sends all requests to Cluster_3.

1. Get the name of the cluster that is closest to your client.

   ```bash
   export CLOSEST_CLUSTER=$(curl -s "https://${WHEREAMI_DNS_PREFIX}.endpoints.${PROJECT_ID}.cloud.goog" | jq -r '.cluster_name')
   echo -e "Closest cluster to you is ${CLOSEST_CLUSTER}"
   ```

   The output is similar to the following (you may see a different cluster based on your client location):

   ```
   Closest cluster to you is gke-west-1
   ```

1. Scale the `whereami-backend` in the closest cluster to 0 to simulate an outage for only the `whereami-backend` app. Access the `whereami` app again and note that now you are routed to the other cluster's backend.

   ```bash
   kubectl --context=${CLOSEST_CLUSTER} -n whereami scale deploy whereami-backend --replicas=0

   sleep 5 # Wait a few seconds until Deployments scale down to 0
   bash << EOF
   for n in {1..10}; do
       curl -s "https://${WHEREAMI_DNS_PREFIX}.endpoints.${PROJECT_ID}.cloud.goog" | jq -r ['.zone,.backend_result.zone] | @csv';
   done
   EOF
   ```

   The output is similar to the following (you may see a different zone based on your client location):

   ```
   "us-west2-a","us-central1-b"
   "us-west2-a","us-central1-b"
   "us-west2-a","us-central1-b"
   "us-west2-a","us-central1-b"
   "us-west2-a","us-central1-b"
   "us-west2-a","us-central1-b"
   "us-west2-a","us-central1-b"
   "us-west2-a","us-central1-b"
   "us-west2-a","us-central1-b"
   "us-west2-a","us-central1-b"
   ```

   Note that the `whereami-frontend` responses are still coming from the closest cluster (since there is a healthy endpoint in that cluster). However, the `whereami-backend` responses are now coming from the cluster in the other region (i.e. `us-central1`). This also shows that locality load balancing is working in the east-west (service to service within the service mesh) direction.

1. Now scale the `whereami-frontend` in the closest cluster to 0 to simulate an outage for the entire `whereami` application (i.e. both frontend and backend are no longer healthy in the closest cluster). Access the `whereami` app again and note that now you are routed to the other cluster's frontend and backend.

   ```bash
   kubectl --context=${CLOSEST_CLUSTER} -n whereami scale deploy whereami-frontend --replicas=0

   sleep 5 # Wait a few seconds until Deployments scale down to 0
   bash << EOF
   for n in {1..10}; do
       curl -s "https://${WHEREAMI_DNS_PREFIX}.endpoints.${PROJECT_ID}.cloud.goog" | jq -r ['.zone,.backend_result.zone] | @csv';
   done
   EOF
   ```

   The output is similar to the following (you may see a different zone based on your client location):

   ```
   "us-central1-b","us-central1-b"
   "us-central1-b","us-central1-b"
   "us-central1-b","us-central1-b"
   "us-central1-b","us-central1-b"
   "us-central1-b","us-central1-b"
   "us-central1-b","us-central1-b"
   "us-central1-b","us-central1-b"
   "us-central1-b","us-central1-b"
   "us-central1-b","us-central1-b"
   "us-central1-b","us-central1-b"
   ```

1. Next, scale just the `whereami-backend` back up in the closest cluster. Keep the `whereami-frontend` scaled down to 0 replicas. Access the `whereami` app again and note that now you are still routed to the other cluster's frontend and backend.

   ```bash
   kubectl --context=${CLOSEST_CLUSTER} -n whereami scale deploy whereami-backend --replicas=3

   kubectl --context=${CLOSEST_CLUSTER} -n whereami wait --for=condition=available deployment whereami-backend

   bash << EOF
   for n in {1..10}; do
       curl -s "https://${WHEREAMI_DNS_PREFIX}.endpoints.${PROJECT_ID}.cloud.goog" | jq -r ['.zone,.backend_result.zone] | @csv';
   done
   EOF
   ```

   The output is similar to the following (you may see a different zone based on your client location):

   ```
   "us-central1-b","us-central1-b"
   "us-central1-b","us-central1-b"
   "us-central1-b","us-central1-b"
   "us-central1-b","us-central1-b"
   "us-central1-b","us-central1-b"
   "us-central1-b","us-central1-b"
   "us-central1-b","us-central1-b"
   "us-central1-b","us-central1-b"
   "us-central1-b","us-central1-b"
   "us-central1-b","us-central1-b"
   ```

   Both responses are coming from the other cluster. The reason for this is flow of the request. Your client calls the `whereami-frontend` first. Since there is no healthy `whereami-frontend` in the closest cluster, the requests are sent to the other cluster's frontend. The `whereami-frontend` in the other cluster then calls the `whereami-backend` Service. From the `whereami-frontend` Pod's perspective, the closest `whereami-backend` is in the same zone (and cluster). Therefore, the request is sent to the closest `whereami-backend` Pod (from the frontend Pod's perspective).

1. Finally, scale the `whereami-frontend` in the closest cluster back up to 3 replicas to bring the `whereami` application back to its original state. Access the `whereami` app again and note that now you are once again routed to the closest cluster. You may also scale everything back up to the normal sunny state.

   ```bash
   kubectl --context=${CLOSEST_CLUSTER} -n whereami scale deploy whereami-frontend --replicas=3
   kubectl --context=${CLUSTER_2} -n whereami scale deploy whereami-frontend --replicas=3
   kubectl --context=${CLUSTER_2} -n whereami scale deploy whereami-backend --replicas=3

   kubectl --context=${CLOSEST_CLUSTER} -n whereami wait --for=condition=available deployment whereami-frontend
   kubectl --context=${CLUSTER_2} -n whereami wait --for=condition=available deployment whereami-frontend
   kubectl --context=${CLUSTER_2} -n whereami wait --for=condition=available deployment whereami-backend
   bash << EOF
   for n in {1..10}; do
       curl -s "https://${WHEREAMI_DNS_PREFIX}.endpoints.${PROJECT_ID}.cloud.goog" | jq -r ['.zone,.backend_result.zone] | @csv';
   done
   EOF
   ```

   The output is similar to the following (you may see a different zone based on your client location):

   ```
   "us-west2-a","us-west2-a"
   "us-west2-a","us-west2-a"
   "us-west2-a","us-west2-a"
   "us-west2-a","us-west2-a"
   "us-west2-a","us-west2-a"
   "us-west2-a","us-west2-a"
   "us-west2-a","us-west2-a"
   "us-west2-a","us-west2-a"
   "us-west2-a","us-west2-a"
   "us-west2-a","us-west2-a"
   ```

## Cleaning up

1. Delete MCI and ILB Gateway configurations.

   ```bash
   # MCI resources for XLB
   kubectl --context ${CLUSTER_CONFIG} -n asm-gateways delete -f ${WORKDIR}/mci.yaml
   kubectl --context ${CLUSTER_CONFIG} -n asm-gateways delete -f ${WORKDIR}/mcs.yaml
   kubectl --context ${CLUSTER_CONFIG} -n asm-gateways delete -f ${WORKDIR}/backend-config.yaml

   # Gateway API resources for ILB
   kubectl --context ${CLUSTER_CONFIG} -n asm-gateways delete -f ${WORKDIR}/gateway-internal-http-ilb.yaml
   kubectl --context ${CLUSTER_CONFIG} -n asm-gateways delete -f ${WORKDIR}/asm-ingressgateway-ilb-httproute.yaml
   ```

1. Delete Cloud Endpoint Services and managed certificates. You cannot delete the project unless you delete all Cloud Endpoint services first.

   ```bash
   gcloud endpoints services delete ${BANK_DNS_PREFIX}.endpoints.${PROJECT_ID}.cloud.goog --async --quiet
   gcloud endpoints services delete ${SHOP_DNS_PREFIX}.endpoints.${PROJECT_ID}.cloud.goog --async --quiet
   gcloud endpoints services delete ${WHEREAMI_DNS_PREFIX}.endpoints.${PROJECT_ID}.cloud.goog --async --quiet
   kubectl --context ${CLUSTER_CONFIG} -n istio-system delete -f ${WORKDIR}/managed-certs.yaml
   ```

1. Verify that the managed certificates are deleted.

   ```bash
   gcloud compute ssl-certificates list
   ```

   The output is similar to the following:

   ```
   Listed 0 items.
   ```

1. Delete Google Service Account created for Workload identity.

   ```bash
   gcloud iam service-accounts remove-iam-policy-binding \
   --role roles/iam.workloadIdentityUser \
   --member "serviceAccount:${PROJECT_ID}.svc.id.goog[bank-of-anthos/${KSA_NAME}]" \
   ${GSA_NAME}@${PROJECT_ID}.iam.gserviceaccount.com
   gcloud iam service-accounts delete ${GSA_NAME}@${PROJECT_ID}.iam.gserviceaccount.com --quiet
   ```

1. Delete GKE clusters.

   ```bash
   cd ${WORKDIR}/asm/infra/terraform/gke-west-1
   gcloud builds submit --config cloudbuild_destroy.yaml --async

   cd ${WORKDIR}/asm/infra/terraform/gke-central-1
   gcloud builds submit --config cloudbuild_destroy.yaml --async

   cd ${WORKDIR}/asm/infra/terraform/gke-central-2
   gcloud builds submit --config cloudbuild_destroy.yaml --async

   cd ${WORKDIR}/asm/infra/terraform/gke-config
   gcloud builds submit --config cloudbuild_destroy.yaml --async
   ```

1. Confirm that all clusters have been successfully deleted.

   ```bash
   gcloud container clusters list
   ```

   The output is similar to the following:

   ```
   Listed 0 items.
   ```

   > Do not proceed until all clusters have been deleted. Re run the command a few times until all clusters are gone.

1. Delete the MCS and Mesh Fleet features.

   ```bash
   cd ${WORKDIR}/asm/infra/terraform/fleet
   gcloud builds submit --config cloudbuild_destroy.yaml
   ```

1. Delete the VPC.

   ```bash
   # Delete the mcsd firewall rules manually first
   gcloud compute firewall-rules delete `gcloud compute firewall-rules list --filter="name ~ ${CLUSTER_2}" --format="value(name)"` --quiet
   gcloud compute firewall-rules delete `gcloud compute firewall-rules list --filter="name ~ ${CLUSTER_3}" --format="value(name)"` --quiet

   # Delete the forwarding rule
   gcloud compute forwarding-rules delete `gcloud compute forwarding-rules list --format="value(name)"` --global --quiet

   # Delete NEGs
   gcloud compute network-endpoint-groups delete `gcloud compute network-endpoint-groups list --format="value(name)" --filter="name ~ 443"` --zone ${CLUSTER_2_ZONE} --quiet
   gcloud compute network-endpoint-groups delete `gcloud compute network-endpoint-groups list --format="value(name)" --filter="name ~ 80"` --zone ${CLUSTER_2_ZONE} --quiet

   # Delete the proxy-only-subnet
   gcloud compute networks subnets delete proxy-only-subnet --region ${CLUSTER_2_REGION} --quiet

   # Delete the VPC
   cd ${WORKDIR}/asm/infra/terraform/vpc
   gcloud builds submit --config cloudbuild_destroy.yaml
   ```

1. Delete the GCLB IP.

   ```bash
   gcloud compute addresses delete gclb-ip --global --quiet
   ```

1. Delete Cloud Armor Security policy.

   ```bash
   gcloud compute security-policies delete gclb-fw-policy --quiet
   ```

1. Confirm endpoints, certificates, hub registration and clusters are deleted.

   ```bash
   gcloud endpoints services list
   gcloud compute security-policies list
   gcloud compute ssl-certificates list
   gcloud container hub memberships list
   gcloud container clusters list
   ```

   The output is similar to the following:

   ```
   Listed 0 items.
   ```

1. Disable Google APIs.

   ```bash
   cd ${WORKDIR}/asm/infra/terraform/api
   gcloud builds submit --config cloudbuild_destroy.yaml
   ```

1. Delete the Terraform state Cloud Storage bucket.

   ```bash
   gcloud storage rm --recursive gs://${PROJECT_ID}
   ```

1. Unset KUBECONFIG

   ```bash
   unset KUBECONFIG
   ```

1. Delete WORKDIR

   ```bash
   cd $HOME && rm -rf $WORKDIR
   ```

## Appendix

### Installing `istioctl` CLI

1. Get `istioctl` CLI.

   ```bash
   curl -LO https://storage.googleapis.com/gke-release/asm/istio-"${ISTIOCTL_VER}"-linux-amd64.tar.gz
   tar xzf istio-${ISTIOCTL_VER}-linux-amd64.tar.gz && rm -rf istio-${ISTIOCTL_VER}-linux-amd64.tar.gz
   export ISTIOCTL_CMD=${WORKDIR}/istio-${ISTIOCTL_VER}/bin/istioctl
   ${ISTIOCTL_CMD} version
   ```

   The output is similar to the following:

   ```
   no running Istio pods in "istio-system"
   1.16.2-asm.2 # Version maybe slightly different
   ```